shader_type canvas_item;
render_mode blend_mix;

const vec4 neutral = vec4(.5,.5,1.0,0.0);

uniform sampler2D brush : hint_normal;

uniform float border = 0.5;
uniform float power = 1.0;

uniform float weight = 1.0;
uniform float neutral_weight = 1.0;
uniform float brush_weight = 1.0;

uniform vec2 brush_uvmult = vec2(1.0);
uniform vec2 brush_uvoffset = vec2(0.0);

void fragment() {
	// dist from center
	vec2 center_uv = (UV - vec2(.5)) * 2.0;
	// size of border
	float d = pow( max(0.0, min(1.0, length( center_uv ) + border)), power );
	// mix with brush
	vec4 brushed = mix( texture( TEXTURE, UV ), texture( brush, (UV*brush_uvmult)+brush_uvoffset ), brush_weight);
	// applying weight
	vec4 weighted = mix( brushed, neutral, max(0.0, min(1.0, (1.0-weight))));
	// applying border 
	vec4 output = mix( weighted, neutral, d);
	// neutral 2 alpha => remove flat normals
	vec2 nxy = (output.xy - vec2(.5))*2.0;
	// mixing output with neutral2alpha
	COLOR = output * vec4(vec3(1.0),  mix( 1.0, length(nxy), max(0.0, min(1.0, (1.0-neutral_weight)))) );
}