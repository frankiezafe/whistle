shader_type canvas_item;
render_mode blend_mix;

const vec4 neutral = vec4(.5,.5,1.0,0.0);

uniform float clear = 1.0;

void fragment() {
	vec4 bg = texture( SCREEN_TEXTURE, SCREEN_UV );
	vec2 diff = (bg.xy-vec2(.5)) * 2.0;
	COLOR = mix( bg, neutral, clear );
}