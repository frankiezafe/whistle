shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform float normal_scale : hint_range(-16,16);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform float radial_freq0 = 1.0;
uniform float radial_freq1 = 1.0;
uniform float normal_min = 0.0;
uniform float normal0_freq = 1.0;
uniform float normal1_freq = 1.0;
uniform float normal0_weight = 1.0;
uniform float normal1_weight = 1.0;
uniform vec2 normal0_scale = vec2(0.0);
uniform vec2 normal1_scale = vec2(0.0);

uniform sampler2D normal0 : hint_normal;
uniform sampler2D normal1 : hint_normal;
uniform sampler2D normal_noise : hint_normal;
uniform sampler2D rt_normal : hint_normal;

const float pi = 3.141592;
const vec3 neutral_normal = vec3(.5,.5,1.);

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	
	vec2 radial = (UV-vec2(.5)) * 2.0;
	float radius = length(radial);
	vec2 mtime = vec2(
		(1.0+sin(radius*radial_freq0+TIME*normal0_freq)) * .5,
		(1.0+sin(radius*radial_freq1+TIME*normal1_freq)) * .5
	);
	vec2 nmixer;
	nmixer.x = normal_min + mtime.x * max(0.0, normal0_weight - normal_min);
	nmixer.y = normal_min + mtime.y * max(0.0, normal1_weight - normal_min);
	float nmixer_l = length( nmixer );
	if ( nmixer_l > 1.0 ) {
		nmixer = normalize(nmixer);
		nmixer_l = 1.0;
	}
	float diff = 1.0 - nmixer_l;
	vec2 n0sc = vec2(1.0) + normal0_scale * mtime.x;
	vec2 n1sc = vec2(1.0) + normal1_scale * mtime.y;
	vec2 nuv0 = (base_uv*n0sc)+n0sc*.5;
	vec2 nuv1 = (base_uv*n1sc)+n1sc*.5;
	vec3 t0 = texture( normal0, nuv0 ).xyz;
	vec3 t1 = texture( normal1, nuv1 ).xyz;
	vec3 norm = t0 * nmixer.x + t1 * nmixer.y + neutral_normal * diff;
	vec2 rtuv = nuv0 - vec2(.5);
	float rtw = max( 0.0, min( 1.0, 1.0 - length( texture( rt_normal, rtuv ).xy ) ));
	norm = mix( norm, texture( rt_normal, rtuv ).xyz, rtw );
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = rtw;
	ROUGHNESS = roughness;
	SPECULAR = specular;
//	NORMALMAP = norm * .7 + texture(normal_noise,nuv0).xyz * .15 + texture(normal_noise,nuv1).xyz * .15;
	NORMALMAP = norm;
	NORMALMAP_DEPTH = normal_scale;
}
