tool

extends Node2D

export (float,0,512) var border:float = 64 setget set_border
export (Vector2) var expand:Vector2 = Vector2.ZERO setget set_expand
export (float,0,360) var orientation:float = 100 setget set_orientation

func set_border(f:float):
	border = f
	refresh()

func set_expand(v2:Vector2):
	expand = v2
	refresh()

func set_orientation(f:float):
	orientation = f
	refresh()

var initialised:bool = false
var tex_size:Vector2 = Vector2.ZERO
var originals:Array = []

func refresh():
	if !initialised:
		return
	# backup of original
	tex_size = get_child(0).texture.get_size()
	if originals.empty():
		for c in get_children():
			var uvs:PoolVector2Array = []
			for uv in c.uv:
				uvs.append(uv)
			originals.append(uvs)
		
	# corners
	for i in range(0,4):
		var n:Polygon2D = get_child(i)
		n.scale = Vector2.ONE * border
		match i:
			0:
				n.position = expand * .5 * Vector2(-1,-1)
			1:
				n.position = expand * .5 * Vector2(1,-1)
			2:
				n.position = expand * .5 * Vector2(1,1)
			3:
				n.position = expand * .5 * Vector2(-1,1)
	# borders
	for i in range(0,4):
		var n:Polygon2D = get_child(i+4)
		match i:
			0:
				n.scale = Vector2.ONE * Vector2(expand.x, border)
				n.position = expand * Vector2.UP * .5
			1:
				n.scale = Vector2.ONE * Vector2(border,expand.y)
				n.position = expand * Vector2.RIGHT * .5
			2:
				n.scale = Vector2.ONE * Vector2(expand.x, border)
				n.position = expand * Vector2.DOWN * .5
			3:
				n.scale = Vector2.ONE * Vector2(border,expand.y)
				n.position = expand * Vector2.LEFT * .5
	# center
	var n:Polygon2D = get_child(8)
	n.position = Vector2.ZERO
	n.scale = expand
	
	var rad:float = orientation / 180 * PI
	var uvoffset:Vector2 = tex_size * .5
	for i in range(0,get_child_count()):
		var c:Polygon2D = get_child(i)
		var original:PoolVector2Array = originals[i]
		c.position = c.position.rotated(rad)
		c.rotation = rad
		var newuv:PoolVector2Array = []
		for j in range(0,original.size()):
			var uv:Vector2 = original[j] - uvoffset
			newuv.append( uvoffset + uv.rotated(rad) )
		c.uv = newuv

func _ready():
	refresh()

func _process(delta):
	if !initialised:
		initialised = true
		refresh()
