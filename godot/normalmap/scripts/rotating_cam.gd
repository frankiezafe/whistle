extends Camera

export (NodePath) var aim_at:NodePath = ""
export (Vector3) var camera_up:Vector3 = Vector3(0,0,-1)
export (Vector3) var camera_right:Vector3 = Vector3(1,0,0)
export (float) var rotation_radius:float = 10
export (float) var rotation_speed:float = 1

onready var target:Spatial = get_node(aim_at)
onready var init_pos:Vector3 = global_transform.origin
var dist:float = 0
var a:float = 0

func _ready():
	camera_up = camera_up.normalized()
	camera_right = camera_right.normalized()
	if target == null:
		return
	dist = (target.global_transform.origin-global_transform.origin).length()

func _process(delta):
	if target == null:
		return
	a += delta * rotation_speed
	var c:float = cos(a) * rotation_radius
	var s:float = sin(a) * rotation_radius
	var newp:Vector3 = init_pos + camera_right * c + camera_up * s
	newp = newp.normalized()
	global_transform.origin = newp * dist
	look_at( target.global_transform.origin, camera_up )
	
