extends Node2D

enum CONTROL { 
	SIZE, STRENGTH, MULT, SIN, CONE, FADEOUT, EXPANDX, EXPANDY, ORIENTATION,
	METALLIC, ROUGHNESS }

export (float,0,256) var brush_size:float = 10

var draw_enabled:bool = false
var controls = []

func get_control( variant ):
	if variant is Control:
		for c in controls: 
			if c.control == variant:
				return c
	elif variant is int:
		for c in controls: 
			if c.usage == variant:
				return c
	return null

func push_value( usage:int, f:float ):
	match usage:
		CONTROL.SIZE:
			$render/mouse.border = f
		CONTROL.STRENGTH:
			$render/mouse.material.set_shader_param('strength',f)
		CONTROL.MULT:
			$brush_vp/brush.material.set_shader_param('mult',f)
		CONTROL.SIN:
			$brush_vp/brush.material.set_shader_param('sin_force',f)
		CONTROL.CONE:
			$brush_vp/brush.material.set_shader_param('cone_force',f)
		CONTROL.FADEOUT:
			$brush_vp/brush.material.set_shader_param('fadeout',f)
		CONTROL.EXPANDX:
			$render/mouse.expand = Vector2(f,$render/mouse.expand.y)
			$preview/mouse.expand = $render/mouse.expand
		CONTROL.EXPANDY:
			$render/mouse.expand = Vector2($render/mouse.expand.x,f)
			$preview/mouse.expand = $render/mouse.expand
		CONTROL.ORIENTATION:
			$render/mouse.orientation = f

func get_value( usage:int ):
	match usage:
		CONTROL.SIZE:
			return $render/mouse.border
		CONTROL.STRENGTH:
			return $render/mouse.material.get_shader_param('strength')
		CONTROL.MULT:
			return $brush_vp/brush.material.get_shader_param('mult')
		CONTROL.SIN:
			return $brush_vp/brush.material.get_shader_param('sin_force')
		CONTROL.CONE:
			return $brush_vp/brush.material.get_shader_param('cone_force')
		CONTROL.FADEOUT:
			return $brush_vp/brush.material.get_shader_param('fadeout')
		CONTROL.EXPANDX:
			return $render/mouse.expand.x
		CONTROL.EXPANDY:
			return $render/mouse.expand.y
		CONTROL.ORIENTATION:
			return $render/mouse.orientation
	return 0

func clear():
#	$render/clear.visible = true
	$render/mouse.visible = true

func _ready():
	$brush_vp.get_texture().flags = Texture.FLAG_FILTER
	push_value( CONTROL.SIZE, brush_size )

func draw_start():
#	$render/clear.visible = false
	$render/mouse.visible = true
	$render.render_target_clear_mode = Viewport.CLEAR_MODE_NEVER

func _process(delta):
	$render/mouse.position = (get_viewport().get_mouse_position()-$preview/texture.position) / $preview/texture.scale

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			draw_start()
		else:
			$render/mouse.visible = false
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_UP:
			pass
		elif event.scancode == KEY_DOWN:
			pass
		elif event.scancode == KEY_BACKSPACE:
			clear()
		elif event.scancode == KEY_ESCAPE:
			get_tree().quit()
