extends Spatial

func _ready():
	$canvas.connect("texture_changed",self,"link_texture")

func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_ESCAPE:
			get_tree().quit()

func link_texture(t:Texture):
	t.flags = Texture.FLAG_REPEAT | Texture.FLAG_FILTER
#	$root/plane.material_override.set_shader_param( "rt_normal", t )
	$root/glass.material_override.set_shader_param( "texture_normal", t )
