tool

extends Sprite

export (float,0,1024) var radius:float = 100 setget set_radius

export (float,0,1) var border:float = 0 setget set_border
export (float,0,20) var power:float = 2 setget set_power
export (float,0,1) var weight:float = 1 setget set_weight
export (float,0,1) var neutral_weight:float = 1 setget set_neutral_weight

export (Array,Texture) var brushes:Array = []

export (int,-1,100) var brush_id:int = -1 setget set_brush_id
export (Texture) var brush:Texture = null setget set_brush
export (float,0,1) var brush_weight:float = 1 setget set_brush_weight
export (Vector2) var brush_uvmult:Vector2 = Vector2.ONE setget set_brush_uvmult
export (Vector2) var brush_uvoffset:Vector2 = Vector2.ZERO setget set_brush_uvoffset

func set_radius(f:float):
	radius = f
	if initialised:
		scale = Vector2.ONE * radius / texture.get_size()

func set_border(f:float):
	border = f
	if initialised:
		material.set_shader_param( "border", border )

func set_power(f:float):
	power = f
	if initialised:
		material.set_shader_param( "power", power )

func set_weight(f:float):
	weight = f
	if initialised:
		material.set_shader_param( "weight", weight )

func set_neutral_weight(f:float):
	neutral_weight = f
	if initialised:
		material.set_shader_param( "neutral_weight", neutral_weight )

func set_brush_id(i:int):
	brush_id = i
	if i < -1:
		brush_id = -1
	elif brush_id >= brushes.size():
		brush_id = brushes.size() -1
	if initialised:
		if brush_id == -1:
			set_brush( null )
		else:
			set_brush( brushes[brush_id] )

func set_brush(t:Texture):
	brush = t
	if initialised:
		print( brush )
		if brush == null:
			material.set_shader_param( "brush", texture )
		else:
			material.set_shader_param( "brush", brush )

func set_brush_weight(f:float):
	brush_weight = f
	if initialised:
		material.set_shader_param( "brush_weight", brush_weight )

func set_brush_uvmult(v2:Vector2):
	brush_uvmult = v2
	if initialised:
		material.set_shader_param( "brush_uvmult", brush_uvmult )

func set_brush_uvoffset(v2:Vector2):
	brush_uvoffset = v2
	if initialised:
		material.set_shader_param( "brush_uvoffset", brush_uvoffset )

var initialised:bool = false

func _process(delta):
	if !initialised:
		initialised = true
		set_radius( radius )
		set_border( border )
		set_power( power )
		set_weight( weight )
		set_neutral_weight( neutral_weight )
		set_brush_id( brush_id )
		set_brush_weight( brush_weight )
		set_brush_uvmult( brush_uvmult )
		set_brush_uvoffset( brush_uvoffset )
