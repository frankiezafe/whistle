extends Node2D

signal texture_changed

export (bool) var autonomous:bool = false
export (bool) var debug:bool = true setget set_debug
export (Vector2) var render_size:Vector2 = Vector2(1024,1024) setget set_render_size
export (int,1,200) var brush_count:int = 1 setget set_brush_count
export (float,0,1) var decay:float = 0

var clear_alpha:float = 1

func set_debug(b:bool):
	debug = b
	if initialised:
		$debug.visible = debug

func set_render_size(v2:Vector2):
	render_size = v2
	if initialised:
		$render.size = v2
		$render/clear.scale = v2 / $render/clear.texture.get_size()
		emit_signal( "texture_changed", $render.get_texture() )

func set_brush_count(i:int):
	brush_count = i
	if initialised:
		while $render.get_child_count() > brush_count + 1:
			$render.remove_child( $render.get_child($render.get_child_count()-1) )
		while $render.get_child_count() < brush_count - 1:
			var b:Sprite = $render.get_child(1).duplicate()
			$render.add_child( b )
		for i in range(1,$render.get_child_count()):
			$render.get_child(i).visible = false
		brush_enabled = []
		for i in range(0,brush_count):
			brush_enabled.append(0)

var initialised:bool = false
var brush_enabled:Array = []
var clear_enabled:int = 0

func clear():
	if !initialised:
		return
	clear_alpha = 1
	$render/clear.material.set_shader_param("clear", clear_alpha)
	clear_enabled = 2

func get_texture():
	if !initialised:
		return null

func get_brush(brushid:int = 0):
	if brushid < 0 or brushid >= brush_count:
		return null
	return $render.get_child(brushid+1)

func move_brush(v2:Vector2, brushid:int = 0):
	if brushid < 0 or brushid >= brush_count:
		return
	$render.get_child(brushid+1).position = v2
	$render.get_child(brushid+1).visible = true
	brush_enabled[brushid] = 2

func _input(event):
	if !initialised or !autonomous:
		return
	if event is InputEventMouseMotion:
		move_brush(event.position, 0)
		move_brush(event.position * 2, 1)
	elif event is InputEventKey and event.pressed:
		if event.scancode == KEY_BACKSPACE:
			clear()

func start():
	initialised = true
	$render.render_target_clear_mode = Viewport.CLEAR_MODE_ALWAYS
	set_debug(debug)
	set_render_size(render_size)
	set_brush_count(brush_count)
	clear()

func _ready():
	set_debug(debug)
	set_render_size(render_size)
	set_brush_count(brush_count)

func _process(delta):
	
	if !initialised:
		start()
		return
	
	if $render.render_target_clear_mode != Viewport.CLEAR_MODE_NEVER:
		$render.render_target_clear_mode = Viewport.CLEAR_MODE_NEVER
	
	for i in range(0,brush_count):
		if brush_enabled[i] > 0:
			brush_enabled[i] -= 1
			if brush_enabled[i] == 0:
				$render.get_child(i+1).visible = false
	
	if clear_enabled > 0:
		clear_enabled -= 1
	elif clear_alpha != decay:
		clear_alpha += (decay-clear_alpha) * min(1,5*delta)
		if abs(decay-clear_alpha) < 1e-4:
			clear_alpha = decay
		$render/clear.material.set_shader_param("clear", clear_alpha)
