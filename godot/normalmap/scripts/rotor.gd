extends Spatial

export (float,0,5) var rotation_speed:float = 1

onready var pivot:Spatial = get_parent()
var drag:bool = false
var motion:Vector2 = Vector2.ZERO

func _process(delta):
	pivot.rotation.y += motion.x * rotation_speed
#	rotation.x -= motion.y * rotation_speed
	motion -= motion * 2 * delta

func _input(event):
	
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		drag = event.pressed
	elif event is InputEventMouseMotion and drag:
		motion += event.relative / get_viewport().size
