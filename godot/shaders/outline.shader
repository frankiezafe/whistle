shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_front,unshaded;
uniform vec4 albedo : hint_color;
uniform float scale = 0;
uniform float offset = 0;

void vertex() {
	VERTEX += NORMAL * scale;
}

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = albedo.rgb;
}
