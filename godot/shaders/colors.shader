shader_type canvas_item;
render_mode blend_mix;

uniform float red_frequency = 1.0;
uniform float green_frequency = 1.0;
uniform float blue_frequency = 1.0;
uniform vec3 rgb = vec3(1.);
uniform float time = 0;

void fragment() {
	COLOR = vec4( 
		vec3( 
			(1.0+cos( time*red_frequency ))*.5, 
			(1.0+cos( time*green_frequency ))*.5, 
			(1.0+cos( time*blue_frequency ))*.5 ) *
			rgb
			, 1.0 );
}