shader_type canvas_item;
render_mode blend_mix;

uniform sampler2D gradient_00 : hint_black;
uniform sampler2D gradient_01 : hint_black;
uniform sampler2D gradient_02 : hint_black;
uniform sampler2D gradient_03 : hint_black;

uniform float uvx_offset = 0.01;

const float PI = 3.1415;

void vertex(){
    UV = VERTEX;
}

void fragment() {
	float ux = UV.x + uvx_offset;
	float uy = UV.y * 4.0;
	if ( uy < 0.5 ) {
		uy += 0.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		COLOR = vec4( mix( texture(gradient_03,vec2(ux,0)).rgb, texture(gradient_00,vec2(ux,0)).rgb, uy ),1.0);
	} else if ( uy < 1.5 ) {
		uy -= 0.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		COLOR = vec4( mix( texture(gradient_00,vec2(ux,0)).rgb, texture(gradient_01,vec2(ux,0)).rgb, uy ),1.0);
	} else if ( uy < 2.5 ) {
		uy -= 1.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		COLOR = vec4( mix( texture(gradient_01,vec2(ux,0)).rgb, texture(gradient_02,vec2(ux,0)).rgb, uy ),1.0);
	} else if ( uy < 3.5 ) {
		uy -= 2.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		COLOR = vec4( mix( texture(gradient_02,vec2(ux,0)).rgb, texture(gradient_03,vec2(ux,0)).rgb, uy ),1.0);
	} else {
		uy -= 3.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		COLOR = vec4( mix( texture(gradient_03,vec2(ux,0)).rgb, texture(gradient_00,vec2(ux,0)).rgb, uy ),1.0);
	}
}