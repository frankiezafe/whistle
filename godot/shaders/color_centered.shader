shader_type canvas_item;
render_mode blend_mix;

uniform float red_frequency = 1.0;
uniform float green_frequency = 1.0;
uniform float blue_frequency = 1.0;
uniform vec3 rgb = vec3(1.);
uniform float time = 0;
uniform float time_shift = 1.0;
uniform float screenratio = 1.0;
uniform float noise = 1.0;

float rand(vec2 n) {
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noiser(vec2 p) {
    vec2 ip = floor(p);
    vec2 u = fract(p);
    u = u*u*(3.0-2.0*u);
    float res = mix(
        mix(rand(ip),rand(ip+vec2(1.0,0.0)),u.x),
        mix(rand(ip+vec2(0.0,1.0)),rand(ip+vec2(1.0,1.0)),u.x),u.y);
    return res*res;
}

void fragment() {
	float d = length((UV - vec2(.5,.5)) * vec2(1.0,screenratio) ) * time_shift;
	float n = noiser( UV * vec2(1.0,screenratio) * 5000.0 + vec2(1000.+TIME) ) * noise;
	COLOR = vec4( 
		vec3( 
			(1.0+cos( (time-d)*red_frequency ))*.5, 
			(1.0+cos( (time-d)*green_frequency ))*.5, 
			(1.0+cos( (time-d)*blue_frequency ))*.5 ) *
			rgb
			, 1.0 );
}