shader_type canvas_item;
render_mode blend_mix;

uniform vec4 color:hint_color;

void fragment() {
	float m = max(color.b,max(color.g,max(color.r,1.)));
	COLOR = color / m;
}