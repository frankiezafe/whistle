shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform float blob = 1.0;

uniform sampler2D gradient_00 : hint_black;
uniform sampler2D gradient_01 : hint_black;
uniform sampler2D gradient_02 : hint_black;
uniform sampler2D gradient_03 : hint_black;
uniform float uvy_offset = 0.01;
uniform float uvy_mult = 1;
const float PI = 3.1415;

varying float pusher;
varying vec3 grad_color;

void vertex() {
	
	float ux = UV.y * uvy_mult + uvy_offset;
	float uy = UV.x * 4.0;
	float push = 0.0;
	
	if ( uy < 0.5 ) {
		uy += 0.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		grad_color = mix( texture(gradient_03,vec2(ux,0)).rgb, texture(gradient_00,vec2(ux,0)).rgb, uy);
		push = grad_color.x;
	} else if ( uy < 1.5 ) {
		uy -= 0.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		grad_color = mix( texture(gradient_00,vec2(ux,0)).rgb, texture(gradient_01,vec2(ux,0)).rgb, uy);
		push = grad_color.x;
	} else if ( uy < 2.5 ) {
		uy -= 1.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		grad_color = mix( texture(gradient_01,vec2(ux,0)).rgb, texture(gradient_02,vec2(ux,0)).rgb, uy);
		push = grad_color.x;
	} else if ( uy < 3.5 ) {
		uy -= 2.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		grad_color = mix( texture(gradient_02,vec2(ux,0)).rgb, texture(gradient_03,vec2(ux,0)).rgb, uy);
		push = grad_color.x;
	} else {
		uy -= 3.5;
		uy = (1.0+sin(-PI*0.5+PI*uy))*0.5;
		grad_color = mix( texture(gradient_03,vec2(ux,0)).rgb, texture(gradient_00,vec2(ux,0)).rgb, uy);
		push = grad_color.x;
	}
//	VERTEX += NORMAL * sin(TIME*10.0-UV.y) * blob;
	VERTEX += NORMAL * push * blob;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	pusher = push;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
//	ALBEDO = albedo.rgb * albedo_tex.rgb;
//	ALBEDO = mix( vec3(1.0,1.0,1.0), vec3(50.0,1.0,7.0), pow(pusher,10.0) );
	ALBEDO = grad_color;
	METALLIC = metallic;
	ROUGHNESS = roughness;
//	METALLIC = grad_color.r;
//	ROUGHNESS = grad_color.g;
	SPECULAR = specular;
}
