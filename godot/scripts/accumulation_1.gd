extends Node2D

export (bool) var random:bool = true setget set_random
export (float,0,50) var color_speed:float = 0.3
export (float,0,50) var scale_speed:float = 5

onready var rcv:Node = $receiver
onready var block:Panel = $tmpl/block
onready var line:HBoxContainer = $tmpl/line
onready var layout:VBoxContainer = $layout
onready var vp_size:Vector2 = get_viewport().get_size()

var is_ready:bool = false
var color:Color = Color.black
var current_block:Panel = null
var current_style:StyleBoxFlat = null
var current_line:HBoxContainer = null

func set_random(b:bool):
	random = b
	if !is_ready:
		return
	while layout.get_child_count() > 0:
		var c:Node = layout.get_child(0)
		layout.remove_child( c )
		c.queue_free()
	new_line()
	new_block()

func _ready():
	is_ready = true
	set_random(random)
	get_viewport().connect("size_changed",self,"vp_changed")
	new_line()
	new_block()
	vp_changed()

func vp_changed():
	vp_size = get_viewport().get_size()
	layout.rect_size = vp_size

func new_line():
	current_line = line.duplicate()
	$layout.add_child( current_line )

func new_block():
	current_block = block.duplicate()
	current_block.visible = true
	current_style = current_block.get( "custom_styles/panel" ).duplicate()
	current_block.set( "custom_styles/panel", current_style )
	current_line.add_child( current_block )

func push( channel, values:Array ):
	match channel:
		rcv.CHANNEL.ALPHA:
			color.r += values[0]*color_speed
			check()
			pass
		rcv.CHANNEL.BETA:
			color.g += values[0]*color_speed
			check()
			pass
		rcv.CHANNEL.DELTA:
			color.b += values[0]*color_speed
			check()
			pass
		rcv.CHANNEL.GAMMA:
			current_block.rect_min_size.x += values[0] * scale_speed
			pass
		rcv.CHANNEL.THETA:
			current_block.rect_min_size.x += values[0] * scale_speed
			pass

func check():
	current_style.bg_color = color
	if color.r >= 1 or color.g >= 1 or color.b >= 1:
		if current_line.rect_size.x > vp_size.x - 20:
			new_line()
		new_block()
		color = Color.black

func _process(delta):
	
	if random:
		randomize()
		var r:float = randf()
		if r <= .33:
			color.r += rand_range(1,5) * delta
		elif r <= .66:
			color.g += rand_range(1,5) * delta
		else:
			color.b += rand_range(1,5) * delta
		current_block.rect_min_size.x += rand_range(0,20) * delta
		current_style.bg_color = color
		check()

