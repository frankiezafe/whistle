extends Node

enum CHANNEL {ALPHA, BETA, DELTA, GAMMA, THETA}

export (int) var port:int = 5000
export (bool) var dump:bool = true

onready var colors:Node = get_parent()
var rcv = null

func _ready():
	rcv = load("res://addons/gdosc/gdoscreceiver.gdns").new()
	rcv.max_queue( 100 )
	rcv.avoid_duplicate( true )
	rcv.setup( port )
	rcv.start()

func _process(delta):
	while( rcv.has_message() ):
		var msg = rcv.get_next()
		if dump:
			print( "address:", msg["address"] )
			print( "typetag:", msg["typetag"] )
			print( "from:" + str( msg["ip"] ) + ":" + str( msg["port"] ) )
			print( "arguments: ")
			for i in range( 0, msg["arg_num"] ):
				print( "\t", i, " = ", msg["args"][i] )
		var addr = msg['address']
		if addr == '/muse/elements/alpha_absolute':
			colors.push( CHANNEL.ALPHA, msg["args"] )
		elif addr == '/muse/elements/beta_absolute':
			colors.push( CHANNEL.BETA, msg["args"] )
		elif addr == '/muse/elements/delta_absolute':
			colors.push( CHANNEL.DELTA, msg["args"] )
		elif addr == '/muse/elements/gamma_absolute':
			colors.push( CHANNEL.GAMMA, msg["args"] )
		elif addr == '/muse/elements/theta_absolute':
			colors.push( CHANNEL.THETA, msg["args"] )
			

func _exit_tree():
	rcv.stop()
