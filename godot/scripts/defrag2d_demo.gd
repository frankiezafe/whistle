extends Node2D

var default = {}

func refresh_display():
	if $defrag.rtype == $defrag.RUNNER_TYPE.ROWER:
		$ui/bg/main/runner.text = "rower"
	elif $defrag.rtype == $defrag.RUNNER_TYPE.ROWERI:
		$ui/bg/main/runner.text = "rower inverted"
	elif $defrag.rtype == $defrag.RUNNER_TYPE.COLUMNER:
		$ui/bg/main/runner.text = "columner"
	elif $defrag.rtype == $defrag.RUNNER_TYPE.COLUMNERI:
		$ui/bg/main/runner.text = "columner inverted"
	$ui/bg/main/num_columns.text = str($ui/bg/main/columns.value)
	$ui/bg/main/num_rows.text = str($ui/bg/main/rows.value)
	$ui/bg/main/num_damping.text = str(int($defrag.damping*1000)*0.001)
	$ui/bg/main/num_diffusion.text = str(int($defrag.diffusion*1000)*0.001)
	$ui/bg/main/num_deflation.text = str(int($defrag.deflation*1000)*0.001)
	$ui/bg/main/num_wpower.text = str(int($defrag.weight_power*1000)*0.001)

func _ready():
	$ui/bg/main/runner.get_popup().add_item("rower")
	$ui/bg/main/runner.get_popup().add_item("rower inverted")
	$ui/bg/main/runner.get_popup().add_item("columner")
	$ui/bg/main/runner.get_popup().add_item("columner inverted")
	$ui/bg/main/columns.value = $defrag.columns
	$ui/bg/main/rows.value = $defrag.rows
	$ui/bg/main/damping.value = $defrag.damping
	$ui/bg/main/diffusion.value = $defrag.diffusion
	$ui/bg/main/deflation.value = $defrag.deflation
	$ui/bg/main/wpower.value = $defrag.weight_power
	refresh_display()
	
	$ui/bg/main/runner.get_popup().connect("index_pressed",self,"runner")
	$ui/bg/main/columns.connect("value_changed",self,"columns")
	$ui/bg/main/rows.connect("value_changed",self,"rows")
	$ui/bg/main/damping.connect("value_changed",self,"damping")
	$ui/bg/main/diffusion.connect("value_changed",self,"diffusion")
	$ui/bg/main/deflation.connect("value_changed",self,"deflation")
	$ui/bg/main/wpower.connect("value_changed",self,"wpower")
	$ui/bg/main/reset.connect("pressed",self,"reset")

func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_ESCAPE:
			get_tree().quit()
		elif event.scancode == KEY_SPACE:
			$ui.visible = !$ui.visible

func runner(i:int):
	match i:
		0:
			$defrag.set_rtype($defrag.RUNNER_TYPE.ROWER)
		1:
			$defrag.set_rtype($defrag.RUNNER_TYPE.ROWERI)
		2:
			$defrag.set_rtype($defrag.RUNNER_TYPE.COLUMNER)
		3:
			$defrag.set_rtype($defrag.RUNNER_TYPE.COLUMNERI)
	refresh_display()

func columns(f:float):
	refresh_display()
func rows(f:float):
	refresh_display()
func damping(f:float):
	$defrag.damping = f
	refresh_display()
func diffusion(f:float):
	$defrag.diffusion = f
	refresh_display()
func deflation(f:float):
	$defrag.deflation = f
	refresh_display()
func wpower(f:float):
	$defrag.weight_power = f
	refresh_display()

func reset():
	$defrag.columns = $ui/bg/main/columns.value
	$defrag.rows = $ui/bg/main/rows.value
	$defrag.set_rtype($defrag.rtype)
	$defrag.set_generate(true)
