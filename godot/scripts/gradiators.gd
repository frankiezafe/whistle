tool

extends Node

export (bool) var rgb:bool = true setget set_rgb
export (int,0,10) var number:int = 4
export (int,1,20) var push_every:int = 1
export (int,2,200) var definition:int = 50
export (int,0,2048) var width:int = 512
export (float,-1,2) var fidelity:float = 1
export (bool) var display:bool = true setget set_display
export (bool) var automatic:bool = true
export (bool) var generate:bool = false setget set_generate
export (bool) var run_in_editor:bool = true

func set_rgb( b:bool ):
	rgb = b
	set_generate( true )

func set_display(b:bool):
	display = b
	for g in gradients:
		g.sprite.visible = display

func set_generate( b:bool ):
	generate = false
	if b:
		gradients = []
		for i in range(0,number):
			gradients.append( {
				'size': definition,
				'values':[],
				'sprite': null,
				'texture': null,
				'gradient': null,
				'refresh': true,
			} )
		while get_child_count() > 0:
			remove_child(get_child(0))
		var gi:int = 0
		for g in gradients:
			if rgb: 
				g.values.resize( g.size * 3 )
			else:
				g.values.resize( g.size )
			for i in range(0,g.size):
				if rgb:
					g.values[i*3] = 0.5
					g.values[i*3+1] = 0.5
					g.values[i*3+2] = 0.5
				else:
					g.values[i] = 0.5
			g.texture = GradientTexture.new()
			g.texture.width = width
			
			g.sprite = Sprite.new()
			g.sprite.centered = false
			g.sprite.scale = Vector2(1,20)
			g.sprite.texture = g.texture
			add_child( g.sprite )
			g.sprite.position = Vector2(10,10+gi)
			g.sprite.visible = display
			gi += 25
		
		accumulators = []
		temporaries = []
		frequencies = []
		for i in range(0,number):
			if rgb:
				temporaries.append( { 'value': [0,0,0], 'hits': 0 } )
				accumulators.append(0)
				accumulators.append(0)
				accumulators.append(0)
			else:
				temporaries.append( { 'value': [0], 'hits': 0 } )
				accumulators.append(0)
			randomize()
			frequencies.append(rand_range(0.1,20))
			frequency_deviation.append(rand_range(5,20))

var gradients:Array = []
var accumulators:Array = []
var temporaries:Array = []
var frequencies:Array = []
var frequency_deviation:Array = []

func reset():
	for g in gradients:
		for i in range(0,len(g.values)):
			g.values[i] = 0.5
		g.refresh = true

func backup_values( g:Dictionary ):
	for i in range( 0, g.size-1):
		var src = (g.size-1)-(i+1)
		var dst = (g.size-1)-i
		if fidelity == 1:
			if rgb:
				g.values[dst*3] = g.values[src*3]
				g.values[dst*3+1] = g.values[src*3+1]
				g.values[dst*3+2] = g.values[src*3+2]
			else:
				g.values[dst] = g.values[src]
		else:
			if rgb:
				g.values[dst*3] += (g.values[src*3]-g.values[dst*3]) * fidelity
				g.values[dst*3+1] += (g.values[src*3+1]-g.values[dst*3+1]) * fidelity
				g.values[dst*3+2] += (g.values[src*3+2]-g.values[dst*3+2]) * fidelity
			else:
				g.values[dst] += (g.values[src]-g.values[dst]) * fidelity

func push( gi:int, red:float, green:float = 0.5, blue:float = 0.5 ):
	if gi < 0 or gi > gradients.size():
		return
	
	if rgb:
		temporaries[gi].value[0]+=red
		temporaries[gi].value[1]+=green
		temporaries[gi].value[2]+=blue
	else:
		temporaries[gi].value[0]+=red
	temporaries[gi].hits += 1
	
	if temporaries[gi].hits >= push_every:
		for i in range(0,len(temporaries[gi].value)):
			temporaries[gi].value[i] /= push_every
		
		var g = gradients[gi]
		backup_values(g)
		g.values[0] = temporaries[gi].value[0]
		if rgb:
			g.values[1] = temporaries[gi].value[1]
			g.values[2] = temporaries[gi].value[2]
		g.refresh = true
		
		for i in range(0,len(temporaries[gi].value)):
			temporaries[gi].value[i] = 0
		temporaries[gi].hits = 0

func _ready():
	set_generate(true)

func _process(delta):
	
	if Engine.editor_hint and !run_in_editor:
		return
	
	var num:int = gradients.size()
	
	if automatic:
		for i in range(0,num): 
			if rgb:
				accumulators[i*3] += delta * ( frequencies[i]+rand_range( -frequency_deviation[i], frequency_deviation[i] ))
				accumulators[i*3+1] += delta * ( frequencies[i]+rand_range( -frequency_deviation[i], frequency_deviation[i] ))
				accumulators[i*3+2] += delta * ( frequencies[i]+rand_range( -frequency_deviation[i], frequency_deviation[i] ))
				push(i, 
					(1+sin(accumulators[i*3]))*0.5,
					(1+sin(accumulators[i*3+1]))*0.5,
					(1+sin(accumulators[i*3+2]))*0.5
					)
			else:
				accumulators[i] += delta * ( frequencies[i]+rand_range( -frequency_deviation[i], frequency_deviation[i] ))
				push(i, (1+sin(accumulators[i]))*0.5 )
	
	for g in gradients:
		if not g.refresh:
			continue
		g.refresh = false
		g.gradient = Gradient.new()
		for i in range(0,g.size):
			var pc = i * 1.0 / (g.size-1)
			if rgb:
				g.gradient.add_point( pc, Color( g.values[i*3], g.values[i*3+1], g.values[i*3+2], 1) )
			else:
				g.gradient.add_point( pc, Color( g.values[i], g.values[i], g.values[i], 1) )
		g.texture.gradient = g.gradient

func get_textures():
	var out = []
	for g in gradients:
		out.append( g.texture )
	return out
