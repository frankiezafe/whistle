extends Panel

var time:float = 0
var time_speed:float = 1
var color_freq_traget:Vector3 = Vector3.ZERO
var color_freq_current:Vector3 = Vector3.ZERO
onready var rcv:Node = $receiver

func _ready():
	var i:int = 0
	material = material.duplicate()
	material.set_shader_param( "rgb", Vector3(1,1,1) )
	for c in $stripes.get_children():
		c.material = c.material.duplicate()
		match i:
			0:
				c.material.set_shader_param( "rgb", Vector3(1,0,0) )
			1:
				c.material.set_shader_param( "rgb", Vector3(0,1,0) )
			2:
				c.material.set_shader_param( "rgb", Vector3(0,0,1) )
		i += 1
	toggle_stripes()

func push( channel, values:Array ):
	match channel:
		rcv.CHANNEL.ALPHA:
			color_freq_traget.x = values[0]
			$ui/red.text = str(values[0])
			pass
		rcv.CHANNEL.BETA:
			color_freq_traget.y = values[0]
			$ui/green.text = str(values[0])
			pass
		rcv.CHANNEL.DELTA:
			color_freq_traget.z = values[0]
			$ui/blue.text = str(values[0])
			pass
		rcv.CHANNEL.GAMMA:
			pass
		rcv.CHANNEL.THETA:
			pass

func _process(delta):
	
	$stripes.rect_size = rect_size
	time += time_speed * delta
	material.set_shader_param( "time", time )
	for c in $stripes.get_children():
		c.material.set_shader_param( "time", time )
	
	color_freq_current += ( color_freq_traget - color_freq_current ) * .4 * delta
	$ui/curr.text = str( int(color_freq_current.x*100)*0.01 ) + ' / '
	$ui/curr.text += str( int(color_freq_current.y*100)*0.01 ) + ' / '
	$ui/curr.text += str( int(color_freq_current.z*100)*0.01 )
	material.set_shader_param( 	"red_frequency", 	color_freq_current.x )
	material.set_shader_param( 	"green_frequency", 	color_freq_current.y )
	material.set_shader_param( 	"blue_frequency", 	color_freq_current.z )
	$stripes/red.material.set_shader_param( 	"red_frequency", 	color_freq_current.x )
	$stripes/green.material.set_shader_param( 	"green_frequency", 	color_freq_current.y )
	$stripes/blue.material.set_shader_param( 	"blue_frequency", 	color_freq_current.z )

func toggle_stripes():
	$stripes.visible = !$stripes.visible
	$ui.visible = $stripes.visible

func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_SPACE:
		toggle_stripes()
