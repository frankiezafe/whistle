tool

extends ImmediateGeometry

export (bool) var volume:bool = false
export (int,2,20) var definition:int = 6
export (float,0,5) var thickness:float = 1
export (bool) var height_control:bool = true
export (float,0,1) var height_max:float = 0
export (float,0,1) var height_grow:float = 1
export (float,0,10) var smooth:float = 1
export (float,0,1) var gyro_mult:float = 0.1
export (float,0,1) var cross:float = 0.1
export (bool) var auto_shrink:bool = false

export (Material) var line_mat:Material
export (Material) var volume_mat:Material
export (NodePath) var gradiator:NodePath setget set_gradiator

var initialised:bool = false
var curve:Curve3D = null
var pt_targets:Array = []

func set_gradiator( np:NodePath ):
	gradiator = np
	initialised = false

func lines():
	material_override = line_mat
	var pts:PoolVector3Array = curve.get_baked_points()
	var ups:PoolVector3Array = curve.get_baked_up_vectors()
	clear()
	begin( Mesh.PRIMITIVE_LINES )
	var l = pts.size()
	var prev:Vector3 = pts[0]
	for i in range(1,l):
		var p = pts[i]
		add_vertex(prev)
		add_vertex(p)
		add_vertex(p)
		add_vertex(p+ups[i]*cross)
		add_vertex(p)
		add_vertex(p-ups[i]*cross)
#		add_vertex(p+Vector3.LEFT*cross)
#		add_vertex(p+Vector3.UP*cross)
#		add_vertex(p+Vector3.DOWN*cross)
#		add_vertex(p+Vector3.FORWARD*cross)
#		add_vertex(p+Vector3.BACK*cross)
		prev = p
	end()

func volume():
	
	material_override = volume_mat
	
	var pts:PoolVector3Array = curve.get_baked_points()
	var ups:PoolVector3Array = curve.get_baked_up_vectors()
	var tilts:PoolRealArray = curve.get_baked_tilts()
	var planes:Array = []
	var ptnum = pts.size()
	var agap:float = TAU / definition
	var total_length = 0
	for i in range(0,ptnum):
		var center:Vector3 = pts[i]
		var x:Vector3 = ups[i]
		var y:Vector3 = Vector3.ZERO
		var z:Vector3 = Vector3.ZERO
		if i == 0:
			z = x.cross(Vector3.UP).normalized()
			y = x.cross(z).normalized()
		else:
			y = (center-pts[i-1]).normalized()
			z = x.cross(y)
		var basis:Basis = Basis(x,y,z)
		if !height_control or (height_control and total_length < height_max):
			var plane = []
			for d in range(0,definition+1):
				var a:float = agap * d
				var offset = basis.xform(Vector3(cos(a),0,sin(a))*thickness)
				var t:Vector3 = z
				if d > definition/4:
					t *= -1
				if d > 3 * definition/4:
					t *= -1
				plane.append([
					center+offset,					# vertex
					offset, 						# normal
					t, 								# tangent
					Vector2(a/TAU,total_length) 	# uv
					])
			planes.append(plane)
		if i > 0:
			total_length += (center-pts[i-1]).length()
	clear()
	begin( Mesh.PRIMITIVE_TRIANGLES )
	
	var pnum = planes.size()
	var uv_div = Vector2(1,total_length)
	
	for i in range(1,pnum):
		
		var prev = planes[i-1]
		var curr = planes[i]
		
		for d in range(0,definition):
			
			var dn = d+1
			
			set_normal( curr[d][1] )
			set_tangent( Plane(curr[d][2],1.0) )
			set_uv( curr[d][3]/uv_div )
			add_vertex( curr[d][0] )
			##### V
			set_normal( prev[d][1] )
			set_tangent( Plane(prev[d][2],1.0) )
			set_uv( prev[d][3]/uv_div )
			add_vertex( prev[d][0] )
			##### V
			set_normal( curr[dn][1] )
			set_tangent( Plane(curr[dn][2],1.0) )
			set_uv( curr[dn][3]/uv_div )
			add_vertex( curr[dn][0] )
			
			set_normal( prev[dn][1] )
			set_tangent( Plane(prev[dn][2],1.0) )
			set_uv( prev[dn][3]/uv_div )
			add_vertex( prev[dn][0] )
			##### V
			set_normal( curr[dn][1] )
			set_tangent( Plane(curr[dn][2],1.0) )
			set_uv( curr[dn][3]/uv_div )
			add_vertex( curr[dn][0] )
			##### V
			set_normal( prev[d][1] )
			set_tangent( Plane(prev[d][2],1.0) )
			set_uv( prev[d][3]/uv_div )
			add_vertex( prev[d][0] )
			
	end()

func _process(delta):
	
	if !initialised:
		initialised = true
		var g:Node = get_node( gradiator )
		if g != null:
			var gs:Array = g.get_textures()
			for i in range( 0, min(4,gs.size()) ):
				var tname = 'gradient_0'+str(i)
				material_override.set_shader_param( tname, gs[i] )
		pt_targets = []
		for c in get_children():
			pt_targets.append( c.translation * Vector3(1,0,1) )
	
	if height_control:
		height_max += height_grow * delta
	
	if get_child_count() > 1:
		curve = Curve3D.new()
		curve.bake_interval = 0.3
		var cnum = get_child_count()
		for i in range(0,cnum):
			var c = get_child(i)
			if !c.visible:
				continue
			var curr = c.translation
			var inv = c.transform.basis.xform(Vector3.DOWN) * smooth
			var outv = c.transform.basis.xform(Vector3.UP) * smooth
			curve.add_point( curr, inv, outv )
		if !volume:
			lines()
		else:
			volume()
	
	if auto_shrink and !Engine.editor_hint:
		for i in range(0, min(get_child_count(),pt_targets.size())):
			var c = get_child(i)
			var t = pt_targets[i]
			var xz:Vector3 = c.translation * Vector3(1,0,1)
			c.translation += (t-xz) * delta
			t -= t * 3 * delta
	
func accel(x:float, y:float, z:float):
	var r:Vector3 = Vector3(y,0,x)
#	$p001.rotation += r * 0.1
#	$p002.rotation += r * 0.2
#	$p003.rotation += r * 0.3

func gyro(x:float, y:float, z:float):
	pt_targets[1].x = x * gyro_mult
	pt_targets[1].z = y * gyro_mult
	pt_targets[2].x = z * gyro_mult
	pt_targets[3].z = x * gyro_mult
#	$p001.translation.x = x * 0.1
#	$p001.translation.z = y * 0.1
#	$p002.translation.x = z * 0.1
#	$p003.translation.z = x * 0.1
