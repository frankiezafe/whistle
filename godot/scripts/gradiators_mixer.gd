tool

extends Polygon2D

export (bool) var reset:bool = false setget do_reset

func do_reset( b:bool ):
	reset = false
	if b:
		initialised = false

var initialised:bool = false

func _process(delta):
	
	if !initialised:
		initialised = true
		var src = get_node('../gradiators')
		print(src)
		if src != null:
			var gs:Array = src.get_textures()
			print( gs )
			for i in range( 0, min(4,gs.size()) ):
				var tname = 'gradient_0'+str(i)
				material.set_shader_param( tname, gs[i] )
