extends Panel

export (float,0,1) var min_channel:float = 0.3

var time:float = 0
var time_speed:float = 1
var color_freq_traget:Vector3 = Vector3.ZERO
var color_freq_current:Vector3 = Vector3.ZERO
onready var rcv:Node = $receiver

func _ready():
	var i:int = 0
	for c in $stripes.get_children():
		c.material = c.material.duplicate()
		match i:
			0:
				c.material.set_shader_param( "rgb", Vector3(1,min_channel,min_channel) )
			1:
				c.material.set_shader_param( "rgb", Vector3(min_channel,1,min_channel) )
			2:
				c.material.set_shader_param( "rgb", Vector3(min_channel,min_channel,1) )
			3:
				c.material.set_shader_param( "rgb", Vector3(1,1,min_channel) )
		i += 1

func push( channel, values:Array ):
	match channel:
		rcv.CHANNEL.ALPHA:
			color_freq_traget.x = values[0]
			$ui/red.text = str(values[0])
			pass
		rcv.CHANNEL.BETA:
			color_freq_traget.y = values[0]
			$ui/green.text = str(values[0])
			pass
		rcv.CHANNEL.DELTA:
			color_freq_traget.z = values[0]
			$ui/blue.text = str(values[0])
			pass
		rcv.CHANNEL.GAMMA:
			pass
		rcv.CHANNEL.THETA:
			pass

func _process(delta):
	
	$stripes.rect_size = rect_size
	time += time_speed * delta
	
	color_freq_current += ( color_freq_traget - color_freq_current ) * .4 * delta
	$ui/curr.text = str( int(color_freq_current.x*100)*0.01 ) + ' / '
	$ui/curr.text += str( int(color_freq_current.y*100)*0.01 ) + ' / '
	$ui/curr.text += str( int(color_freq_current.z*100)*0.01 )
	
	var vps:Vector2 = get_viewport().get_size()
	var ratio:float = vps.x / vps.y
	
	var i:int = 0
	for c in $stripes.get_children():
		c.material.set_shader_param( "time", time )
		c.material.set_shader_param( "screenratio", vps.x / vps.y )
		match i:
			3:
				c.material.set_shader_param( 	"red_frequency", 	color_freq_current.x * color_freq_current.y )
				c.material.set_shader_param( 	"green_frequency", 	color_freq_current.x * color_freq_current.y )
				c.material.set_shader_param( 	"blue_frequency", 	color_freq_current.z )
			_:
				c.material.set_shader_param( 	"red_frequency", 	color_freq_current.x )
				c.material.set_shader_param( 	"green_frequency", 	color_freq_current.y )
				c.material.set_shader_param( 	"blue_frequency", 	color_freq_current.z )
		i += 1

func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_SPACE:
		$ui.visible = !$ui.visible
