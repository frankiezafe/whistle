tool

extends Spatial

enum RUNNER_TYPE {
	ROWER,
	ROWERI,
	COLUMNER,
	COLUMNERI,
	SPIRAL_0,
	SPIRAL_1,
	SPIRAL_2,
	SPIRAL_3
}

export (Vector3) var domain:Vector3 = Vector3(10,1,10) 	setget set_domain
export (int,1,100) var columns:int = 5 					setget set_columns
export (int,1,100) var rows:int = 5 					setget set_rows
export (Vector2) var padding:Vector2 = Vector2.ZERO
export (Vector2) var walkway:Vector2 = Vector2.ZERO
export (float,0,1) var damping:float = 0.1
export (float,0,20) var deflation:float = 0.01
export (float,0,1) var diffusion:float = 0.01
export (float,0.1,4) var weight_power:float = 1.05
export (RUNNER_TYPE) var rtype:int = RUNNER_TYPE.ROWER	setget set_rtype
export (bool) var generate:bool = false 				setget set_generate
export (bool) var pump:bool = false
export (bool) var run:bool = false

var initialised:bool = false
var cell_size:Vector3 = Vector3.ONE
var cell_offset:Vector3 = Vector3.ONE
var cells:Array = []
var current_line:int = -1
var current_cell:int = -1
var strength:float = 1

var runner:Dictionary = {
	'id':-1,
	'column':0,
	'row':0,
	'dirx': 0,
	'diry': 0
}

func set_domain(v3:Vector3):
	domain = v3
	cell_size = domain / Vector3(rows,1,columns)
	cell_offset = domain * Vector3(-.5,0,-.5) 

func set_rtype(i:int):
	rtype = i
	runner.id = -1
	match rtype:
		RUNNER_TYPE.ROWER:
			runner.column = 0
			runner.row = 0
			runner.dirx = 1
			runner.diry = 0
		RUNNER_TYPE.ROWERI:
			runner.column = columns-1
			runner.row = rows-1
			runner.dirx = -1
			runner.diry = 0
		RUNNER_TYPE.COLUMNER:
			runner.column = 0
			runner.row = 0
			runner.dirx = 0
			runner.diry = 1
		RUNNER_TYPE.COLUMNERI:
			runner.column = columns-1
			runner.row = rows-1
			runner.dirx = 0
			runner.diry = -1

func set_columns(i:int):
	columns = i
	initialised = false
	
func set_rows(i:int):
	rows = i
	initialised = false

func set_generate(b:bool):
	generate = false
	if b:
		initialised = false

func build():
	set_domain( domain )
	var b:Spatial = $tmpl/block
	while $blocks.get_child_count() > 0:
		var c:Spatial = $blocks.get_child(0)
		$blocks.remove_child( c )
		c.queue_free()
	cells = []
	for y in rows:
		for x in columns:
			var nb:Spatial = b.duplicate()
			nb.material_override = b.material_override.duplicate()
			$blocks.add_child(nb)
			cells.append({
				'node': nb,
				'scale': Vector3.ONE,
				'weight': 1,
				'wpow': 1
			})

func render():
	
	var lweights:Array = []
	var totalweights:float = 0
	for y in rows:
		var yw:float = 0
		for x in columns:
			var id:int = x + y * columns
			if cells[id].weight < 0.01:
				cells[id].weight = 0.01
			cells[id].wpow = pow(cells[id].weight,weight_power)
			yw += cells[id].wpow
		lweights.append(yw)
		totalweights += yw
	
	var ypos:float = 0
	for y in rows:
		
		var xpos:float = 0
		var h:float = lweights[y] / totalweights * domain.z
		
		for x in columns:
			
			var c:Dictionary = cells[x + y * columns]
			var w:float = c.wpow / lweights[y] * domain.x
			var elevation:float = c.wpow / totalweights * domain.y
			c.node.translation.x = xpos + w * .5
			c.node.translation.y = elevation * .5
			c.node.translation.z = ypos + h * .5
			c.node.translation += cell_offset
			c.node.scale.x = c.scale.x * w - padding.x - c.wpow / lweights[y] * walkway.x
			c.node.scale.y = elevation
			c.node.scale.z = c.scale.z * h - padding.y - c.wpow / lweights[y] * walkway.y
			c.node.material_override.set_shader_param("radiance",min(1.2,c.wpow*50.0/totalweights))
			if c.node.scale.x < 0 or c.node.scale.y < 0 or c.node.scale.z < 0:
				c.node.visible = false
			else:
				c.node.visible = true
			
			xpos += w
		ypos += h

func _ready():
	if !Engine.editor_hint:
		initialised = false
		run = true

func _process(delta):
	
	if !run:
		return
	
	if !initialised:
		get_viewport().get_texture().flags = 0
		build()
		render()
		initialised = true
	
	if pump:
		
		if runner.id == -1 or rand_range(0,1) < .1:
			
			runner.id = runner.column + runner.row * columns
			runner.column += runner.dirx
			runner.row += runner.diry
			match rtype:
				
				RUNNER_TYPE.ROWER:
					if runner.column >= columns:
						runner.column = 0
						runner.row += 1
					if runner.row >= rows:
						runner.row = 0
				RUNNER_TYPE.ROWERI:
					if runner.column <= 0:
						runner.column = columns-1
						runner.row -= 1
					if runner.row < 0:
						runner.row = rows-1
				
				RUNNER_TYPE.COLUMNER:
					if runner.row >= rows:
						runner.row = 0
						runner.column += 1
					if runner.column >= columns:
						runner.column = 0
				RUNNER_TYPE.COLUMNERI:
					if runner.row < 0:
						runner.row = rows-1
						runner.column -= 1
					if runner.column < 0:
						runner.column = columns-1
			
			current_cell = runner.id
			current_line = current_cell % columns
			strength = rand_range(-5,50)
		for i in range(0,cells.size()):
			var c:Dictionary = cells[i]
			if i == current_cell:
				var nw:float = c.weight + strength
				c.weight += (nw-c.weight) * (1-damping) * delta
				if c.weight < 1:
					c.weight = 1
				var col = c.node.material_override.get_shader_param("rgb")
				if col == null:
					col = Vector3.ZERO
				col += Vector3(rand_range(0,1),rand_range(0,1),rand_range(0,1))*delta
				c.node.material_override.set_shader_param("rgb",col)
			else:
				if c.weight > 1:
					c.weight -= deflation * delta
				if diffusion > 0:
					var up:int = i - columns
					var down:int = i + columns
					if up > -1:
						var dw:float = (cells[up].weight - c.weight) * diffusion * delta
						if dw < 0:
							cells[up].weight -= dw
							c.weight += dw
						elif dw > 0:
							cells[up].weight += dw
							c.weight -= dw
					if down < cells.size():
						var dw:float = (cells[down].weight - c.weight) * diffusion * delta
						if dw < 0:
							cells[down].weight -= dw
							c.weight += dw
						elif dw > 0:
							cells[down].weight += dw
							c.weight -= dw
		render()
