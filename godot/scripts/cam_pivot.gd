extends Spatial

var current_y_rotation:float = 0
var target_y_rotation:float = 0
var current_y_speed:float = 0
var target_y_speed:float = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if current_y_rotation != target_y_rotation:
		var prevy:float = current_y_rotation
		current_y_rotation += (target_y_rotation-current_y_rotation) * min(0.3, delta)
		rotation_degrees.y += current_y_rotation - prevy
	if current_y_speed != target_y_speed:
		current_y_speed += (target_y_speed-current_y_speed) * min(0.3, delta)
	rotation_degrees.y += current_y_speed * delta
	
