extends Node2D

export (Vector2) var block_size:Vector2 = Vector2(10,10)

onready var block:Panel = $tmpl/block
onready var vp_size:Vector2 = get_viewport().get_size()

var refresh_req:bool = false
var blocks:Array = []

func reset():
	$vpc/vp.render_target_update_mode = Viewport.UPDATE_DISABLED
	while $vpc/vp.get_child_count():
		var c:Node = $vpc/vp.get_child(0)
		$vpc/vp.remove_child( c )
		c.queue_free()
	blocks = []
	for i in range(0,4):
		var d:Dictionary = {
			'node': block.duplicate(),
			'size': Vector2(block_size),
			'direction':Vector2.ZERO
		}
		blocks.append( d )
		add_child( d.node )
	blocks[0].node.rect_position = Vector2.ZERO
	blocks[0].direction.x = 1
	blocks[1].node.rect_position = Vector2( vp_size.x - blocks[0].size.x, 0 )
	blocks[1].direction.y = 1
	blocks[2].node.rect_position = Vector2( vp_size.x - blocks[0].size.x, vp_size.y - blocks[0].size.y )
	blocks[2].direction.x = -1
	blocks[3].node.rect_position = Vector2( 0, vp_size.y - blocks[0].size.y )
	blocks[3].direction.y = -1
	refresh_req = false

func _ready():
	get_viewport().connect("size_changed",self,"vp_changed")
	vp_changed()

func vp_changed():
	vp_size = get_viewport().get_size()
	$vpc.rect_size = vp_size
	$vpc/vp.size = vp_size
	$vpc/vp.render_target_update_mode = Viewport.UPDATE_ALWAYS
	refresh_req = true
	

func _process(delta):
	if refresh_req:
		reset()
	for b in blocks:
		b.node.rect_position += b.direction * 15 * delta
