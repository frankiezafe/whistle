extends Spatial

var pipe_debug = preload( "res://materials/debug.material" )
var pipe_mixer = preload( "res://materials/mixer.material" )

func _ready():
	$ui/main/osc.pressed = !$gradiators.automatic
	$ui/main/dump.pressed = $receiver.dump
	$ui/main/apply_minmax.pressed = $receiver.apply_minmax
	set_minmax()
	$receiver.connect("minmax_changed",self,"set_minmax")
	$ui/main/gradients.pressed = $gradiators.display
	gradients_display()
	$ui/main/curve_smooth.value = $blobby.smooth
	$ui/main/curve_reactivity.value = $blobby.gyro_mult
	$ui/main/fidelity.value = $gradiators.fidelity
	$ui/main/thickness.value = $blobby.thickness
	$ui/main/blob.value = $blobby.material_override.get_shader_param("blob")

func _input(event):
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_SPACE:
			$ui.visible = !$ui.visible

func gradients_display():
	if $gradiators.display:
		$ui.position.y = $gradiators.number * 25 + 10
	else:
		$ui.position.y = 0

func _on_dump_pressed():
	$receiver.dump = $ui/main/dump.pressed

func set_minmax():
	for i in range(0,$receiver.minmax.size()):
		if $receiver.minmax[i][0] == null:
			get_node( "ui/main/min/"+str(i) ).text = 'null'
		else:
			get_node( "ui/main/min/"+str(i) ).text = str(int($receiver.minmax[i][0]*100)*0.01)
		if $receiver.minmax[i][1] == null:
			get_node( "ui/main/max/"+str(i) ).text = 'null'
		else:
			get_node( "ui/main/max/"+str(i) ).text = str(int($receiver.minmax[i][1]*100)*0.01)

func _on_fidelity(value):
	$gradiators.fidelity = value

func _on_blob(value):
	$blobby.material_override.set_shader_param("blob", value)

func _on_thickness(value):
	$blobby.thickness = value

func _on_gradients_pressed():
	$gradiators.display = $ui/main/gradients.pressed
	gradients_display()

func _on_osc():
	$receiver.reset()
	$gradiators.reset()
	$gradiators.automatic = !$ui/main/osc.pressed

func _on_rot_speed_changed(value):
	$scenery/cam_pivot.target_y_speed = value

func _on_rot_changed(value):
	$scenery/cam_pivot.target_y_rotation = value

func _on_apply_minmax():
	$receiver.apply_minmax = $ui/main/apply_minmax.pressed

func _on_curve_smooth(value):
	$blobby.smooth = $ui/main/curve_smooth.value

func _on_curve_reactivity(value):
	$blobby.gyro_mult = $ui/main/curve_reactivity.value

func _on_worm():
	if $ui/main/worm.pressed:
		$blobby.volume_mat = pipe_mixer
	else:
		$blobby.volume_mat = pipe_debug
	$blobby.initialised = false
