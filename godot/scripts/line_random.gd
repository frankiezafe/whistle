tool

extends Sprite

export (int) var history:int = 20 setget set_history
export (float,0,20) var speed:float = 5

func set_history( i:int ):
	history = i

var values:Array = []
var grad:Gradient = null
var a:float = 0

func _process(delta):
	
	randomize()
	a += delta * speed * rand_range(0.5,1)
	
	if grad == null:
		texture = GradientTexture.new()
		texture.width = 512
	
	if values.size() != history:
		values.resize( history )
		for i in range( 0,history ):
			values[i] = 0
			var pc = i * 1.0 / (history-1)
		texture.gradient = grad
	
	for i in range( 0, history-1):
		var src = (history-1)-(i+1)
		var dst = (history-1)-i
		values[dst] = values[src]
	
	values[0] = (1+sin(a)) * 0.5
	grad = Gradient.new()
	for i in range(0,history):
		var pc = i * 1.0 / (history-1)
		grad.add_point( pc, Color( values[i], values[i], values[i], 1) )
	texture.gradient = grad
