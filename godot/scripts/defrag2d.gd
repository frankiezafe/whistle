tool

extends Node2D

enum RUNNER_TYPE {
	ROWER,
	ROWERI,
	COLUMNER,
	COLUMNERI,
	SPIRAL_0,
	SPIRAL_1,
	SPIRAL_2,
	SPIRAL_3
}

export (int,1,100) var columns:int = 5 					setget set_columns
export (int,1,100) var rows:int = 5 					setget set_rows
export (float,0,1) var damping:float = 0.1
export (float,0,2) var deflation:float = 0.01
export (float,0,1) var diffusion:float = 0.01
export (float,0.5,4) var weight_power:float = 1.05
export (RUNNER_TYPE) var rtype:int = RUNNER_TYPE.ROWER	setget set_rtype
export (bool) var generate:bool = false 				setget set_generate
export (bool) var pump:bool = false
export (bool) var run:bool = false

var initialised:bool = false
var cells:Array = []
var current_line:int = -1
var current_cell:int = -1
var strength:float = 1

var runner:Dictionary = {
	'id':-1,
	'column':0,
	'row':0,
	'dirx': 0,
	'diry': 0
}

func set_rtype(i:int):
	rtype = i
	runner.id = -1
	match rtype:
		RUNNER_TYPE.ROWER:
			runner.column = 0
			runner.row = 0
			runner.dirx = 1
			runner.diry = 0
		RUNNER_TYPE.ROWERI:
			runner.column = columns-1
			runner.row = rows-1
			runner.dirx = -1
			runner.diry = 0
		RUNNER_TYPE.COLUMNER:
			runner.column = 0
			runner.row = 0
			runner.dirx = 0
			runner.diry = 1
		RUNNER_TYPE.COLUMNERI:
			runner.column = columns-1
			runner.row = rows-1
			runner.dirx = 0
			runner.diry = -1

func set_columns(i:int):
	columns = i
	initialised = false
	
func set_rows(i:int):
	rows = i
	initialised = false

func set_generate(b:bool):
	generate = false
	if b:
		initialised = false

func build():
	var b:Node2D = $tmpl/block
	for c in cells:
		$blocks.remove_child( c.node )
#		c.node.queue_free()
	cells = []
	for y in rows:
		for x in columns:
			var nb:Node2D = b.duplicate()
			nb.material = nb.material.duplicate()
			$blocks.add_child(nb)
			cells.append({
				'node': nb,
				'scale': Vector2.ONE / nb.texture.get_size(),
				'weight': 1,
				'wpow': 1
			})

func render():
	var lweights:Array = []
	var totalweights:float = 0
	for y in rows:
		var yw:float = 0
		for x in columns:
			var id:int = x + y * columns
			if cells[id].weight < 0.01:
				cells[id].weight = 0.01
			cells[id].wpow = pow(cells[id].weight,weight_power)
			yw += cells[id].wpow
		lweights.append(yw)
		totalweights += yw
	var gsize:Vector2 = get_viewport().size
	var ypos:float = 0
	for y in rows:
		var xpos:float = 0
		var h:float = lweights[y] / totalweights * gsize.y
		for x in columns:
			var c:Dictionary = cells[x + y * columns]
			var w:float = c.wpow / lweights[y] * gsize.x
			c.node.position.x = xpos
			c.node.position.y = ypos
			c.node.scale.x = c.scale.x * w
			c.node.scale.y = c.scale.y * h
			xpos += w
		ypos += h

func _ready():
	if !Engine.editor_hint:
		initialised = false
		run = true

func _process(delta):
	
	if !run:
		return
	
	if !initialised:
		build()
		render()
		initialised = true
	
	if pump:
		
		if runner.id == -1 or rand_range(0,1) < .1:
			
			runner.id = runner.column + runner.row * columns
			runner.column += runner.dirx
			runner.row += runner.diry
			match rtype:
				
				RUNNER_TYPE.ROWER:
					if runner.column >= columns:
						runner.column = 0
						runner.row += 1
					if runner.row >= rows:
						runner.row = 0
				RUNNER_TYPE.ROWERI:
					if runner.column <= 0:
						runner.column = columns-1
						runner.row -= 1
					if runner.row < 0:
						runner.row = rows-1
				
				RUNNER_TYPE.COLUMNER:
					if runner.row >= rows:
						runner.row = 0
						runner.column += 1
					if runner.column >= columns:
						runner.column = 0
				RUNNER_TYPE.COLUMNERI:
					if runner.row < 0:
						runner.row = rows-1
						runner.column -= 1
					if runner.column < 0:
						runner.column = columns-1
			
			current_cell = runner.id
			current_line = current_cell % columns
			strength = rand_range(-5,50)
		for i in range(0,cells.size()):
			var c:Dictionary = cells[i]
			if i == current_cell:
				var nw:float = c.weight + strength
				c.weight += (nw-c.weight) * (1-damping) * delta
				if c.weight < 1:
					c.weight = 1
				var col = c.node.material.get_shader_param('color')
				if col == null:
					col = Color.black
				col += Color(rand_range(0,1),rand_range(0,1),rand_range(0,1),0)*delta
				c.node.material.set_shader_param('color',col)
			else:
				if c.weight > 1:
					c.weight -= deflation * delta
				if diffusion > 0:
					var up:int = i - columns
					var down:int = i + columns
					if up > -1:
						var dw:float = (cells[up].weight - c.weight) * diffusion * delta
						if dw < 0:
							cells[up].weight -= dw
							c.weight += dw
						elif dw > 0:
							cells[up].weight += dw
							c.weight -= dw
					if down < cells.size():
						var dw:float = (cells[down].weight - c.weight) * diffusion * delta
						if dw < 0:
							cells[down].weight -= dw
							c.weight += dw
						elif dw > 0:
							cells[down].weight += dw
							c.weight -= dw
		render()
