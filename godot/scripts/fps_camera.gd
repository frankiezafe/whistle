extends Camera

onready var Yaw = get_parent()

## Increase this value to give a slower turn speed
export (float,0,1000) var TURN_INERTIA:float = 200
export (float,0,100) var MOVE_SPEED:float = 5
export (bool) var FREE:bool = true
export (bool) var CAPTURE_MOUSE:bool = true

var dirs:Array = [
	[false,KEY_W,KEY_UP],
	[false,KEY_D,KEY_RIGHT],
	[false,KEY_S,KEY_DOWN],
	[false,KEY_A,KEY_LEFT],
]

func toggle_captive_mouse():
	CAPTURE_MOUSE = !CAPTURE_MOUSE
	if CAPTURE_MOUSE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _ready():
	## Tell Godot that we want to handle input
	set_process_input(true)

func _process(delta):
	
	var gt = self
	if !FREE:
		gt = Yaw
	
	for i in range(0,4):
		if not dirs[i][0]:
			continue
		if i == 0:
			Yaw.translation += gt.global_transform.basis.xform( Vector3.FORWARD ) * MOVE_SPEED * delta
		elif i == 1:
			Yaw.translation += gt.global_transform.basis.xform( Vector3.RIGHT ) * MOVE_SPEED * delta
		elif i == 2:
			Yaw.translation += gt.global_transform.basis.xform( Vector3.BACK ) * MOVE_SPEED * delta
		elif i == 3:
			Yaw.translation += gt.global_transform.basis.xform( Vector3.LEFT ) * MOVE_SPEED * delta

func look_updown_rotation(rotation = 0):
	"""
  Returns a new Vector3 which contains only the x direction
  We'll use this vector to compute the final 3D rotation later
	"""
	var toReturn = self.get_rotation() + Vector3(rotation, 0, 0)

	##
	## We don't want the player to be able to bend over backwards
	## neither to be able to look under their arse.
	## Here we'll clamp the vertical look to 90° up and down
	toReturn.x = clamp(toReturn.x, PI / -2, PI / 2)

	return toReturn

func look_leftright_rotation(rotation = 0):
	"""
  Returns a new Vector3 which contains only the y direction
  We'll use this vector to compute the final 3D rotation later
	"""
	return Yaw.get_rotation() + Vector3(0, rotation, 0)

func mouse(event):
	if CAPTURE_MOUSE:
		"""
		First person camera controls
		"""
		##
		## We'll use the parent node "Yaw" to set our left-right rotation
		## This prevents us from adding the x-rotation to the y-rotation
		## which would result in a kind of flight-simulator camera
		Yaw.set_rotation(look_leftright_rotation(event.relative.x / -TURN_INERTIA))

		##
		## Now we can simply set our y-rotation for the camera, and let godot
		## handle the transformation of both together
		self.set_rotation(look_updown_rotation(event.relative.y / -TURN_INERTIA))

func _input(event):
	##
	## We'll only process mouse motion events
	if event is InputEventMouseMotion:
		return mouse(event)
	elif event is InputEventKey:
		for d in dirs:
			for i in range(1,len(d)):
				if d[i] == event.scancode:
					d[0] = event.pressed
		if event.scancode == KEY_ESCAPE and event.pressed:
			get_tree().quit()
		elif event.scancode == KEY_SPACE and event.pressed:
			toggle_captive_mouse()

func _enter_tree():
	if CAPTURE_MOUSE:
		"""
		Hide the mouse when we start
		"""
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _leave_tree():
	if CAPTURE_MOUSE:
		"""
		Show the mouse when we leave
		"""
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
