extends MeshInstance

onready var rot_speed:Vector3 = Vector3.ZERO
var right_pressed:bool = false

func _process(delta):
	rotation += rot_speed * delta
	rot_speed -= rot_speed * 5 * delta

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		right_pressed = event.pressed
	if right_pressed:
		if event is InputEventMouseMotion:
			rot_speed.x += event.relative.y * 0.1
			rot_speed.y += event.relative.x * 0.1
