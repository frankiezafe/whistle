extends Node

signal dump
signal minmax_changed

export (int) var port:int = 5000
export (bool) var dump:bool = true
export (float,-10,10) var min_value:float = 0
export (float,-10,10) var max_value:float = 1
export (bool) var apply_minmax:bool = true
export (bool) var auto_adjust:bool = true
export (bool) var alpha_process:bool = true
export (bool) var beta_process:bool = true
export (bool) var delta_process:bool = true
export (bool) var gamma_process:bool = true
export (bool) var theta_process:bool = true
export (bool) var accel_process:bool = true
export (bool) var gyro_process:bool = true
export (NodePath) var gradiators:NodePath = ""
export (NodePath) var blob:NodePath = ""

var rcv = null
var time:float = 0
onready var g:Node = get_node(gradiators)
onready var b:Node = get_node(blob)
onready var minmax:Array = [[null,null],[null,null],[null,null]] 

func _ready():
	rcv = load("res://addons/gdosc/gdoscreceiver.gdns").new()
	rcv.max_queue( 100 )
	rcv.avoid_duplicate( true )
	rcv.setup( port )
	rcv.start()

func reset():
	minmax = [[null,null],[null,null],[null,null]]
	emit_signal("minmax_changed")

func map( channel:int, f:float ):
	if channel < 0 or channel >= minmax.size():
		return f
	if f < -1000 or f > 100:
		return 0
	var mm:Array = minmax[channel]
	if auto_adjust:
		if mm[0] == null or mm[0] > f:
			mm[0] = f
			emit_signal("minmax_changed")
		if mm[1] == null or mm[1] < f:
			mm[1] = f
			emit_signal("minmax_changed")
	if !apply_minmax:
		return min(1,max(0,f))
	if mm[0] == mm[1]:
		return 0
	return (f-mm[0])/(mm[1]-mm[0])

func _process(delta):
	
	while( rcv.has_message() ):
		var msg = rcv.get_next()
		if dump:
			emit_signal("dump", msg['address'] )
			print( "address:", msg["address"] )
			print( "typetag:", msg["typetag"] )
			print( "from:" + str( msg["ip"] ) + ":" + str( msg["port"] ) )
			print( "arguments: ")
			for i in range( 0, msg["arg_num"] ):
				print( "\t", i, " = ", msg["args"][i] )
		
		var addr = msg['address']
		if g != null:
			if addr == '/muse/elements/alpha_absolute' and alpha_process:
				g.push(2,
					map(0,msg["args"][0]),
					map(1,msg["args"][1]),
					map(2,msg["args"][2]))
			elif addr == '/muse/elements/beta_absolute' and beta_process:
				g.push(3,
					map(0,msg["args"][0]),
					map(1,msg["args"][1]),
					map(2,msg["args"][2]))
			elif addr == '/muse/elements/delta_absolute' and delta_process:
				g.push(0,
					map(0,msg["args"][0]),
					map(1,msg["args"][1]),
					map(2,msg["args"][2]))
			elif addr == '/muse/elements/gamma_absolute' and gamma_process:
				g.push(4,
					map(0,msg["args"][0]),
					map(1,msg["args"][1]),
					map(2,msg["args"][2]))
			elif addr == '/muse/elements/theta_absolute' and theta_process:
				g.push(1,
					map(0,msg["args"][0]),
					map(1,msg["args"][1]),
					map(2,msg["args"][2]))
			elif addr == '/muse/acc' and accel_process:
				if b != null:
					b.accel( 
						msg["args"][0],
						msg["args"][1],
						msg["args"][2])
			elif addr == '/muse/gyro' and gyro_process:
				if b != null:
					b.gyro( 
						msg["args"][0],
						msg["args"][1],
						msg["args"][2])

func _exit_tree():
	rcv.stop()
