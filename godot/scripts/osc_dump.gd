extends Label

export (NodePath) var osc_receiver:NodePath = ""
export (int,1,100) var max_stack:int = 20
var stack:Array = []

onready var rcv = get_node(osc_receiver)

func _ready():
	if rcv != null:
		rcv.connect( "dump", self, "push" )

func push( s:String ):
	stack.append(s)
	while stack.size() > max_stack:
		stack.pop_front()
	if stack.empty():
		text = "nieks ke dalle"
	else:
		text = ""
		for s in stack:
			text += s + '\n'
