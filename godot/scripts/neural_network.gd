tool

extends ImmediateGeometry

export (int,1,10) var roots:int = 3 setget set_roots
export (int,10,10000) var max_branches:int = 2000
export (bool) var clear:bool = false setget set_clear
export (bool) var process:bool = false
export (float,0,5) var random_dir:float = 0
export (float,0,1) var shrink_ampl:float = 0
export (float,0,50) var shrink_freq:float = 0
export (float,0,50) var reactivity:float = 1
export (Vector3) var wind:Vector3 = Vector3.ZERO

func set_clear(b:bool):
	clear = false
	if b:
		initialised = false

func set_roots(i:int):
	roots = i
	initialised = false

var initialised:bool = false
var previous_translation:Vector3 = Vector3.ZERO
var trees:Array = []
var leaves:Array = []
var angle:float = 0
var time:float = 0

func rand_v3():
	return Vector3(rand_range(-1,1),rand_range(-1,1),rand_range(-1,1)).normalized()

func update_pos( t:Dictionary ):
	if t.root_id == -1:
		t.pos = t.dir * t.length
	else:
		t.pos = trees[t.root_id].pos + t.dir * t.length

func new_branch( dir: Vector3, root_id:int = -1  ):
	if trees.size() >= max_branches:
		return
	var t:Dictionary = {
		'root_id': root_id,
		'id': trees.size(),
		'dir': dir,
		'extra_dir': Vector3.ZERO,
		'speed': 1,
		'length': 0,
		'rand': rand_range(0.2,1),
		'fertility': 0,
		'pos': Vector3.ZERO,
		'pull': Vector3.ZERO,
		'depth': 0,
		'white':0
	}
	if root_id == -1:
		t.fertility = 1
	else:
		t.fertility = trees[t.root_id].fertility * t.rand * .5
		t.depth = trees[t.root_id].depth + 1
	if t.depth > 6:
		leaves.append( t.id )
	update_pos(t)
	trees.append( t )

func split(t):
	if t.fertility < 1e-5:
		return
	var num:int = int(rand_range(1,roots))
	for i in range(0,num):
		var d:Vector3 = t.dir + rand_v3() * .3
		new_branch( d.normalized(), t.id )

func new_breed():
	randomize()
	for i in range(0,roots):
		new_branch( rand_v3() )

func _process(delta):
	
	if !process:
		return
	
	if !initialised:
		initialised = true
		previous_translation = translation
		trees = []
		leaves = []
		$leaves.multimesh.instance_count = 0
		$leaves.multimesh.visible_instance_count = 0
		new_breed()
	
	angle += delta * shrink_freq
	time += delta
	
	var delta_trans:Vector3 = previous_translation - translation
	previous_translation = translation
	
	clear()
	begin(Mesh.PRIMITIVE_LINES)
	var w:float = 1.0
	var consumed_pull:Vector3 = Vector3.ZERO
	var theoric_pos:Vector3 = Vector3.ZERO
	for t in trees:
		if t.speed > 0:
			t.speed -= delta
			t.speed = max(0,t.speed)
			t.pos += t.dir * delta
			t.length += delta * t.speed
			if t.speed <= 0:
				t.speed = 0
				split(t)
		t.pos += delta_trans
		t.pos += wind * (1+sin(time*t.id))*.5 * delta / (t.depth+1)
		t.dir = ( t.dir + rand_v3() * random_dir * delta).normalized()
		theoric_pos = t.dir * t.length * (1 + sin(angle*t.rand) * shrink_ampl * t.depth)
		if t.root_id != -1:
			theoric_pos += trees[t.root_id].pos
		t.pull = (theoric_pos - t.pos)-t.pull
		consumed_pull = t.pull * min( 1, reactivity * (1+t.rand) * delta)
		t.pos += consumed_pull
		t.pull -= consumed_pull
		t.white = t.pull.length()
		if t.root_id == -1:
			set_color( Color(1,1,1) )
			add_vertex( Vector3.ZERO )
		else:
			w = 1.0 / (trees[t.root_id].depth+1)
			set_color( Color(trees[t.root_id].white,1-w,1-w,.6) )
			add_vertex( trees[t.root_id].pos )
		w = 1.0 / (t.depth+1)
		set_color( Color(t.white,1-w,1-w,.6) )
		add_vertex( t.pos )
	end()
	
	if leaves.size() != $leaves.multimesh.instance_count:
		$leaves.multimesh.instance_count = leaves.size()
		$leaves.multimesh.visible_instance_count = leaves.size()
	var lid:int = 0
	for i in leaves:
		var t:Dictionary = trees[i]
		w = 1.0 / (t.depth+1)
		$leaves.multimesh.set_instance_color( lid, Color(t.white,1-w,1-w) )
		$leaves.multimesh.set_instance_transform( lid, Transform(Basis.IDENTITY.scaled(Vector3.ONE*t.length), t.pos) )
		lid += 1
