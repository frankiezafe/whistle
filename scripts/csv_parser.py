import os,csv,datetime
import bpy

DATA_PATH = "/home/frankiezafe/forge.blender/whistle/docs/mindMonitor_2020-10-25--18-19-49.csv"
TIMESTAMP_LABEL = 'TimeStamp'
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
COLUMNS = [
	{'csv':'Delta_TP9', 'prop':'delta'},
	{'csv':'Theta_TP9', 'prop':'theta'},
	{'csv':'Alpha_TP9', 'prop':'alpha'},
	{'csv':'Beta_TP9', 'prop':'beta'}
	]
SMOOTH_WINDOW_SEC = 6
EXPORT_PATH = "/home/frankiezafe/forge.blender/whistle/docs/mm.csv"

OBJECT = "Cube"

class signal():
	
	name = None
	property = None
	index = None
	data = None
	average = None
	start = None
	duration = None
	min = None
	max = None
	
	def __init__(self, name, property, index):
		self.name = name
		self.property = property
		self.index = index
		self.data = []
	
	def push( self, timestamp, cell ):
		if cell == '':
			return
		ts = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f').timestamp()
		self.data.append( [
			ts,				# 0: timestamp (seconds since epoch)
			float(cell),	# 1: value
			0,				# 2: percentage between min & max, require process to be called
			0,				# 3: smoothed percentage
			0,				# 4: smoothed percentage min
			0				# 5: smoothed percentage max
			] )
	
	def process(self):
		
		if self.data == None or len(self.data) == 0:
			return
		ld = len(self.data)
		self.start = self.data[0][0]
		self.duration = self.data[ld-1][0] - self.start
		
		# averaging and normalisation
		self.average = 0
		for d in self.data:
			v = d[1]
			d[0] -= self.start
			if self.min == None or self.min > v:
				self.min = v
			if self.max == None or self.max < v:
				self.max = v
			self.average += v
		for d in self.data:
			d[2] = (d[1]-self.min) / (self.max-self.min)
		self.average /= ld
		
		# smoothing
		for i in range(0,len(self.data)):
			d = self.data[i]
			wdata = []
			wdata.append( d[2] )
			# seeking backward
			b = i-1
			while b > -1:
				prevd = self.data[b]
				if d[0] - prevd[0] > SMOOTH_WINDOW_SEC:
					break
				wdata.append( prevd[2] )
				b -= 1
			# seeking forwaard
			b = i+1
			while b < len(self.data):
				nextd = self.data[b]
				if nextd[0] - d[0] > SMOOTH_WINDOW_SEC:
					break
				wdata.append( nextd[2] )
				b += 1
			d[4] = None
			d[5] = None
			for v in wdata:
				d[3] += v
				if d[4] == None or d[4] > v:
					d[4] = v
				if d[5] == None or d[5] < v:
					d[5] = v
			d[3] /= len(wdata)
	
	def dump(self, full = True):
		print(self.name + ' (' +self.property+')' )
		print('\t'+'index:', self.index)
		print('\t'+'start:', self.start)
		print('\t'+'duration:', self.duration)
		print('\t'+'average:', self.average)
		print('\t'+'min:', self.min)
		print('\t'+'max:', self.max)
		print('\t'+'data:', len(self.data))
		if not full:
			return
		for i in range(0,len(self.data)):
			print( '\t\t', i, ': ts:', self.data[i][0], ', value:', self.data[i][1], ', pc:', self.data[i][2])

class analyser():
	
	signals = None
	data_length = None
	duration = None
	
	def __init__(self):
		self.signals = []
		f = open(DATA_PATH,'r')
		reader = csv.reader(f)
		ts_id = -1
		rnum = 0
		for row in reader:
			# first row MUST contains the columns rows
			if rnum == 0:
				labels = row
				for i in range( 0, len(labels) ):
					if labels[i] == TIMESTAMP_LABEL:
						ts_id = i
					else:
						for c in COLUMNS:
							if labels[i] == c['csv']:
								self.signals.append( signal(c['csv'],c['prop'],i) )
								break
			else:
				if ts_id == -1:
					break
				ts = row[ts_id]
				for s in self.signals:
					s.push( ts, row[s.index] )
			rnum += 1
		f.close()
		for s in self.signals:
			s.process()
			if self.data_length == None or self.data_length < len(s.data):
				self.data_length = len(s.data)
			if self.duration == None or self.duration < s.duration:
				self.duration = s.duration
	
	def export(self,path):
		f = open(path,'w')
		for s in self.columns:
			f.write( s.name + '_ts,' )
			f.write( s.name + '_value,' )
			f.write( s.name + '_pc,' )
			f.write( s.name + '_smooth,' )
			f.write( s.name + '_smin,' )
			f.write( s.name + '_smax,' )
		f.write('\n')
		for i in range(0,self.data_length):
			for s in self.signals:
				if i >= len(c.data):
					for v in s.data[0]:
						f.write( '0,' )
				else:
					for v in s.data[i]:
						f.write( str(v)+',' )
			f.write('\n')
		f.close()
	
	def dump(self, full=True):
		for s in self.signals:
			s.dump(full)

print( "############ NEW RUN ############ " )

an = analyser()
an.dump(False)

context = bpy.context
scene = context.scene

scene.frame_end = an.duration*bpy.context.scene.render.fps

bpy.ops.object.select_all(action='DESELECT')
obj = bpy.data.objects[ OBJECT ]
obj.select_set(True)
bpy.context.active_object.animation_data_clear()

for s in an.signals:
	obj[s.property+'_raw'] = 0.0
	obj[s.property+'_smooth'] = 0.0
	obj[s.property+'_smin'] = 0.0
	obj[s.property+'_smax'] = 0.0
	for d in s.data:
		obj[ s.property+'_raw' ] = d[2]
		obj.keyframe_insert( data_path='["'+s.property+'_raw'+'"]', frame=d[0]*bpy.context.scene.render.fps )
		obj[ s.property+'_smooth' ] = d[3]
		obj.keyframe_insert( data_path='["'+s.property+'_smooth'+'"]', frame=d[0]*bpy.context.scene.render.fps )
		obj[ s.property+'_smin' ] = d[4]
		obj.keyframe_insert( data_path='["'+s.property+'_smin'+'"]', frame=d[0]*bpy.context.scene.render.fps )
		obj[ s.property+'_smax' ] = d[5]
		obj.keyframe_insert( data_path='["'+s.property+'_smax'+'"]', frame=d[0]*bpy.context.scene.render.fps )

fcurves = obj.animation_data.action.fcurves
for fcurve in fcurves:
    for kf in fcurve.keyframe_points:
        kf.interpolation = 'LINEAR'

'''
print( bpy.context.scene.render.fps )
print( obj["alpha"] )
obj.keyframe_insert(data_path='["alpha"]', frame=10.0)
'''
