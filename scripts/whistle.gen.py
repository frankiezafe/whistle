import os
from datetime import datetime
import bpy
import bmesh
import mathutils
import math

'''
/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of whistle
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2020 frankiezafe.org
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */
 '''

'''
uncomment *_RANGE paramaters to generate a serie of whistle
'''

DIAMETER = 10
#DIAMETER_RANGE = [10,15,1] # min, max, step
PINCH_THICKNESS = 1.5
LENGTH = 100
#LENGTH_RANGE = [50,150,5] # min, max, step
WALL = 1.2
INTAKE_OFFSET = 10
INTAKE_ANGLE = 45
#INTAKE_ANGLE_RANGE = [20,60,5] # min, max, step
REDUCER_OFFSET = 2
TMPL_DIAMETER = 10.4
CLEANUP = True
EXPORT_STL = True
EXPORT_FOLDER = "generated"

TMPL = "template"

# utils to copy collection
def copy_objects(from_col, to_col, linked):
	for o in from_col.objects:
		dupe = o.copy()
		if not linked and o.data:
			dupe.data = dupe.data.copy()
		to_col.objects.link(dupe)

def copy(parent, collection, linked=True):
	cc = bpy.data.collections.new(collection.name)
	copy_objects(collection, cc, linked)
	for c in collection.children:
		copy(cc, c, linked)
	parent.children.link(cc)

# gathering and validation of pipe modifiers
class pipe_modifiers:
	
	label = None  
	bottom = None
	hole = None
	pipe_inner = None
	bottom = None
	pinch = None
	valid = False
	
	def __init__(self):
		return
	
	def load(self, mod):
		if mod.name.startswith('label'):
			self.label = mod
		elif mod.name.startswith('bottom'):
			self.bottom = mod
		elif mod.name.startswith('hole'):
			self.hole = mod
		elif mod.name.startswith('pipe_inner'):
			self.pipe_inner = mod
		elif mod.name.startswith('bottom'):
			self.bottom = mod
		elif mod.name.startswith('pinch'):
			self.pinch = mod
	
	def validate(self):
		self.valid = False
		if self.label == None:
			print('missing modifier label!')
			return
		elif self.bottom == None:
			print('missing modifier bottom!')
			return
		elif self.hole == None:
			print('missing modifier hole!')
			return
		elif self.pipe_inner == None:
			print('missing modifier pipe_inner!')
			return
		elif self.bottom == None:
			print('missing modifier bottom!')
			return
		elif self.pinch == None:
			print('missing modifier pinch!')
			return
		self.valid = True

	def show(self, enable):
		if not self.valid:
			return
		self.label.show_viewport = enable
		self.bottom.show_viewport = enable
		self.hole.show_viewport = enable
		self.pipe_inner.show_viewport = enable
		self.bottom.show_viewport = enable
		self.pinch.show_viewport = enable

class pipe_config:
	
	DIAMETER = 10
	PINCH_THICKNESS = 1.5
	LENGTH = 100
	WALL = 1.2
	INTAKE_OFFSET = 20
	INTAKE_ANGLE = 45
	REDUCER_OFFSET = 3
	TMPL_DIAMETER = 10.4
	CLEANUP = True
	EXPORT_STL = True
	EXPORT_FOLDER = "generated"
	
	def __init__(self):
		return
	
	def load(self, dic):
		if 'DIAMETER' in dic:
			self.DIAMETER = dic['DIAMETER']
		if 'PINCH_THICKNESS' in dic:
			self.PINCH_THICKNESS = dic['PINCH_THICKNESS']
		if 'LENGTH' in dic:
			self.LENGTH = dic['LENGTH']
		if 'WALL' in dic:
			self.WALL = dic['WALL']
		if 'INTAKE_OFFSET' in dic:
			self.INTAKE_OFFSET = dic['INTAKE_OFFSET']
		if 'INTAKE_ANGLE' in dic:
			self.INTAKE_ANGLE = dic['INTAKE_ANGLE']
		if 'REDUCER_OFFSET' in dic:
			self.REDUCER_OFFSET = dic['REDUCER_OFFSET']
		if 'TMPL_DIAMETER' in dic:
			self.TMPL_DIAMETER = dic['TMPL_DIAMETER']
		if 'CLEANUP' in dic:
			self.CLEANUP = dic['CLEANUP']
		if 'EXPORT_STL' in dic:
			self.EXPORT_STL = dic['EXPORT_STL']
		if 'EXPORT_FOLDER' in dic:
			self.EXPORT_FOLDER = dic['EXPORT_FOLDER']
	
# gathering, validation and processing of whistle
class whistle:
	
	collection = None
	pipe = None
	pipe_cap = None
	pipe_capi = -1
	pipe_mods = None
	pinch = None
	intake = None
	arc = None
	pipe_inner = None
	pipe_inner_mod = None
	reducer = None
	flat_bottom = None
	blower_cutters = None
	bottom_cutters = None
	editable = None
	text = None
	txt = None
	valid = False
	timestamp = None
	uname = None
	conf = None
	
	def __init__(self, collection):
		self.collection = collection
		self.conf = pipe_config()
		self.unique_name()
		return
	
	def configure( self, dic ):
		self.conf.load( dic )
		self.unique_name()
	
	def validate(self):
		self.valid = False
		if self.pipe == None:
			print('missing pipe!')
			return
		elif self.pipe_cap == None:
			print('missing pipe_cap!')
			return
		elif self.pinch == None:
			print('missing pinch!')
			return
		elif self.intake == None:
			print('missing intake!')
			return
		elif self.arc == None:
			print('missing arc!')
			return
		elif self.pipe_inner == None:
			print('missing pipe_inner!')
			return
		elif self.pipe_inner_mod == None:
			print('missing pipe_inner_mod!')
			return
		elif self.reducer == None:
			print('missing reducer!')
			return
		elif self.flat_bottom == None:
			print('missing flat_bottom!')
			return
		elif self.blower_cutters == None:
			print('missing blower_cutters!')
			return
		elif self.bottom_cutters == None:
			print('missing bottom_cutters!')
			return
		elif self.editable == None:
			print('missing editable!')
			return
		elif self.text == None:
			print('missing text!')
			return
		elif self.txt == None:
			print('missing txt!')
			return
		self.pipe_mods.validate()
		if not self.pipe_mods.valid:
			print('invalid pipe!')
			return
		self.valid = True
		print( 'Whistle is valid, all objects required have been found' )
	
	def load(self, obj):
		
		if not obj:
			return
		
		on = obj.name
		
		if on.startswith('pipe_inner'):
			self.pipe_inner = obj
			# loading modifiers
			for m in self.pipe_inner.modifiers:
				if m.name == 'cutter':
					self.pipe_inner_mod = m
		
		elif on.startswith('pipe'):
			self.pipe = obj
			# loading vertex groups
			vgi = 0
			for vg in self.pipe.vertex_groups:
				if vg.name == 'cap':
					self.pipe_cap = vg
					self.pipe_capi = vgi
				vgi += 1
			# loading modifiers
			self.pipe_mods = pipe_modifiers()
			for m in self.pipe.modifiers:
				self.pipe_mods.load(m)
				
		elif on.startswith('pinch'):
			self.pinch = obj
		
		elif on.startswith('intake'):
			self.intake = obj
		
		elif on.startswith('arc'):
			self.arc = obj
		
		elif on.startswith('reducer'):
			self.reducer = obj
		
		elif on.startswith('flat_bottom'):
			self.flat_bottom = obj
		
		elif on.startswith('blower_cutters'):
			self.blower_cutters = obj
		
		elif on.startswith('bottom_cutters'):
			self.bottom_cutters = obj
		
		elif on.startswith('editable'):
			self.editable = obj
		
		elif on.startswith('text'):
			self.text = obj
		
		elif on.startswith('txt'):
			self.txt = obj
	
	def delete(self, o):
		bpy.ops.object.select_all(action='DESELECT')
		o.select_set(True)
		bpy.ops.object.delete()
	
	def unique_name(self):
		now = datetime.now()
		self.uname = 'whistle.'
		self.uname += now.strftime("%Y%m%d%H%M%S")
		self.uname += '.D'+str(self.conf.DIAMETER)+'.L'+str(self.conf.LENGTH)+'.W'+str(self.conf.WALL)+'.T'+str(self.conf.PINCH_THICKNESS)+'.A'+str(self.conf.INTAKE_ANGLE)+'.O'+str(self.conf.INTAKE_OFFSET)+'.R'+str(self.conf.REDUCER_OFFSET)
		self.timestamp = now.strftime("%Y/%m/%d %H:%M:%S")
		
	def process(self):
		if not self.valid:
			return
		
		# removing unused
		self.delete( self.blower_cutters )
		self.delete( self.bottom_cutters )
		self.delete( self.text )
		self.delete( self.editable )
		self.delete( self.txt )
		
		# deleting the pinch
		bpy.ops.object.select_all(action='DESELECT')
		pn = self.pinch.name
		self.pinch.select_set(True)
		bpy.ops.object.delete()
		
		# creating a good pinch
		bpy.ops.mesh.primitive_torus_add(align='WORLD', location=(0,0,0), rotation=(0, 0, 0), major_segments=64, minor_segments=24, major_radius=(self.conf.DIAMETER+self.conf.WALL*2)*0.5, minor_radius=self.conf.PINCH_THICKNESS)
		self.pinch = bpy.context.active_object
		self.pinch.name = pn
		bpy.ops.object.modifier_add(type='BOOLEAN')
		self.pinch.modifiers[0].name = 'cutter'
		self.pinch.modifiers[0].object = self.flat_bottom
		self.collection.objects.link(self.pinch)
		bpy.context.scene.collection.objects.unlink(self.pinch)
		
		# relinking modifiers
		self.pipe_mods.label.object = None
		self.pipe_mods.bottom.object = self.arc
		self.pipe_mods.hole.object = self.intake
		self.pipe_mods.pipe_inner.object = self.pipe_inner
		self.pipe_mods.pinch.object = self.pinch
		self.pipe_mods.show( False )
		self.pipe_inner_mod.object = self.reducer
		
		# scaling
		sc = (self.conf.DIAMETER+self.conf.WALL*2)/self.conf.TMPL_DIAMETER
		self.pipe.scale = self.pipe.scale * sc
		self.pipe_inner.dimensions = [self.conf.DIAMETER, self.conf.DIAMETER, self.conf.LENGTH*2]
		self.arc.dimensions = [self.conf.DIAMETER-0.1, self.conf.DIAMETER-0.1, self.arc.dimensions[2]]
		
		c = math.cos(math.radians(self.conf.INTAKE_ANGLE))
		s = math.sin(math.radians(self.conf.INTAKE_ANGLE))
		self.intake.dimensions = [self.conf.DIAMETER*3, self.conf.DIAMETER*3, self.conf.DIAMETER*3 * s/c]
		self.intake.location[2] = self.conf.INTAKE_OFFSET + 0.01
		self.reducer.dimensions = [self.conf.DIAMETER*3, self.conf.DIAMETER*3, self.conf.DIAMETER*3]
		self.reducer.location[1] = self.conf.DIAMETER*0.5 - self.conf.REDUCER_OFFSET
		self.reducer.location[2] = self.conf.INTAKE_OFFSET
		
		self.flat_bottom.dimensions = [self.conf.DIAMETER*3, self.conf.DIAMETER*3, self.conf.DIAMETER*3]
		
		# adjusting top cap
		minz = None
		std_verts = []
		cap_verts = []
		for v in self.pipe.data.vertices:
			if len(v.groups) < self.pipe_capi:
				std_verts.append(v)
				continue
			keepon = False
			for g in v.groups:
				if g.group == self.pipe_capi:
					keepon = True
					break
			if not keepon:
				std_verts.append(v)
				continue
			cap_verts.append(v)
			if minz == None or minz > v.co[2]:
				minz = v.co[2]
		# minz is in local space => influenced by self.pipe.scale
		zmove = ((minz*self.pipe.scale[2])-self.conf.LENGTH) / self.pipe.scale[2]
		for v in cap_verts:
			v.co[2] -= zmove
		for v in std_verts:
			# rescaling z
			pc = v.co[2] / minz
			v.co[2] = pc * self.conf.LENGTH / self.pipe.scale[2]
		self.pipe_mods.show( True )
		
		if self.conf.CLEANUP:
			
			# removing & applying modifiers on pipe
			bpy.ops.object.select_all(action='DESELECT')
			self.pipe.select_set(True)
			bpy.context.view_layer.objects.active = self.pipe
			mnames = []
			for m in self.pipe.modifiers:
				if m.object != None:
					mnames.append( m.name )
				else:
					bpy.ops.object.modifier_remove(modifier=m.name)
			for mn in mnames:
				print( 'applying mod: ' + mn )
				bpy.ops.object.modifier_apply(modifier=mn)
			
			# removing all construction objects
			self.delete( self.reducer )
			self.delete( self.flat_bottom )
			self.delete( self.intake )
			self.delete( self.pipe_inner )
			self.delete( self.pinch )
			self.delete( self.arc )
			
			# finalising the pipe
			bpy.ops.object.select_all(action='DESELECT')
			self.pipe.select_set(True)
			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.mesh.select_all(action='SELECT')
			bpy.ops.mesh.remove_doubles()
			bpy.ops.mesh.select_all(action='SELECT')
			bpy.ops.mesh.normals_make_consistent(inside=False)
			bpy.ops.mesh.select_all(action='SELECT')
			bpy.ops.mesh.quads_convert_to_tris()
			bpy.ops.object.mode_set(mode='OBJECT')
			
		self.pipe.name = self.uname
		
		if self.conf.EXPORT_STL:
			bpy.ops.object.select_all(action='DESELECT')
			self.pipe.select_set(True)
			# preparing export folder
			folder = ""
			if self.conf.EXPORT_FOLDER.startswith('/'):
				folder = os.path.join( self.conf.EXPORT_FOLDER )
			else:
				folder = os.path.join( bpy.path.abspath('//'), self.conf.EXPORT_FOLDER )
			if not os.path.isdir(folder):
				os.makedirs(folder)
			fp = os.path.join( folder, self.uname + '.stl' )
			# exporting stl
			bpy.ops.export_mesh.stl(filepath=fp, check_existing=False, use_selection=True )
			# generating report
			fp = os.path.join( folder, self.uname + '.txt' )
			f = open( fp, 'w' )
			f.write( 'name: ' + self.uname + '\n' )
			f.write( 'timestamp: ' + self.timestamp + '\n' )
			f.write( 'inner diameter: ' + str(self.conf.DIAMETER) + 'cm\n' )
			f.write( 'outer diameter: ' + str(self.conf.DIAMETER+self.conf.WALL*2) + 'cm\n' )
			f.write( 'total length: ' + str(self.conf.LENGTH) + 'cm\n' )
			f.write( 'chamber length: ' + str(self.conf.LENGTH-self.conf.INTAKE_OFFSET) + 'cm\n' )
			f.write( 'chamber volume: ' + str(self.conf.DIAMETER*math.pi* (self.conf.LENGTH-self.conf.INTAKE_OFFSET)) + 'cm3\n' )
			f.write( 'wall thickness: ' + str(self.conf.WALL) + 'cm\n' )
			f.write( 'intake position: ' + str(self.conf.INTAKE_OFFSET) + 'cm\n' )
			f.write( 'intake angle: ' + str(self.conf.INTAKE_ANGLE) + '°\n' )
			f.write( 'reducer offset: ' + str(self.conf.REDUCER_OFFSET) + 'cm\n' )
			f.write( 'cleanup: ' + str(self.conf.CLEANUP) + '\n' )
			f.close()

# main #############
context = bpy.context
scene = context.scene

# creation of all configurations
tmpl_conf = {
	'DIAMETER': DIAMETER,
	'PINCH_THICKNESS': PINCH_THICKNESS,
	'LENGTH': LENGTH,
	'WALL': WALL,
	'INTAKE_OFFSET': INTAKE_OFFSET,
	'INTAKE_ANGLE': INTAKE_ANGLE,
	'REDUCER_OFFSET': REDUCER_OFFSET,
	'TMPL_DIAMETER': TMPL_DIAMETER,
	'CLEANUP': CLEANUP,
	'EXPORT_STL': EXPORT_STL,
	'EXPORT_FOLDER': EXPORT_FOLDER
}
configs = []
if 'DIAMETER_RANGE' in locals():
	print( "generation of whistles with diameters from", DIAMETER_RANGE[0], "to", DIAMETER_RANGE[1] )
	for d in range( DIAMETER_RANGE[0], DIAMETER_RANGE[1]+1, DIAMETER_RANGE[2] ):
		cc = tmpl_conf.copy()
		cc['DIAMETER'] = d
		print(cc)
		configs.append(cc)
elif 'LENGTH_RANGE' in locals():
	print( "generation of whistles with length from", LENGTH_RANGE[0], "to", LENGTH_RANGE[1] )
	for l in range( LENGTH_RANGE[0], LENGTH_RANGE[1]+1, LENGTH_RANGE[2] ):
		cc = tmpl_conf.copy()
		cc['LENGTH'] = l
		print(cc)
		configs.append(cc)
elif 'INTAKE_ANGLE_RANGE' in locals():
	print( "generation of whistles with intake angle from", INTAKE_ANGLE_RANGE[0], "to", INTAKE_ANGLE_RANGE[1] )
	for a in range( INTAKE_ANGLE_RANGE[0], INTAKE_ANGLE_RANGE[1]+1, INTAKE_ANGLE_RANGE[2] ):
		cc = tmpl_conf.copy()
		cc['INTAKE_ANGLE'] = a
		print(cc)
		configs.append(cc)
else:
	configs.append(tmpl_conf)
	
for config in configs:
	
	# hide all collections
	all_colls = []
	for c in bpy.data.collections:
		c.hide_select = True
		c.hide_render = True
		c.hide_viewport = True
		all_colls.append(c.name)
	
	# getting the TMPL collection
	coll = bpy.data.collections[TMPL]
	
	# duplicate the template collection
	assert(coll is not scene.collection)
	parent_col = context.scene.collection
	copy(scene.collection, coll, False)
	curr_coll = None
	for c in bpy.data.collections:
		if not c.name in all_colls:
			curr_coll = c
	
	# adjusting the whistle
	if curr_coll != None:
		w = whistle( curr_coll )
		w.configure( config )
		for o in curr_coll.objects:
			w.load(o)
		w.validate()
		w.process()
