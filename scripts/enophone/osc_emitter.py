import sys, time
import bluetooth
import numpy as np
from pythonosc import osc_message_builder
from pythonosc import udp_client

bt_host = "F4:0E:11:75:6F:E6"
bt_port = 1

osc_ip = "127.0.0.1"
osc_port = 25000

# Create the client socket
sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bt_host, bt_port))

r = sock.recv(50)#
bucket = b''+r
row_count = 0

osc_client = udp_client.SimpleUDPClient(osc_ip,osc_port)

def row_data( r ):
	global osc_client
	if len(r) != 20:
		print( "error: not enough data in row" )
		return
	values = []
	for i in range(0,len(r)//4):
		values.append( int.from_bytes( r[i*4:(i+1)*4], "big", signed="False") )
	for i in range(0,5):
		if i == 0:
			values[i] %= 2**16
		else:
			values[i] = (values[i] // 256) * 5. / 2**23
	msg = osc_message_builder.OscMessageBuilder(address='/eno')
	for i in range(0,len(values)):
		if i == 0:
			msg.add_arg(values[i], arg_type='i')
		else:
			msg.add_arg(values[i], arg_type='f')
	msg = msg.build()
	osc_client.send(msg)
	#print( values )

def analyse_data():
	global bucket
	global row_count
	start = 0
	while bucket.find(b'bS') > -1:
		start = bucket.find(b'bS')
		end = bucket.find(b'bS',start+2)
		if end == -1:
			break
		row = bucket[start:end]
		row_data(row)
		row_count += 1
		bucket = bucket[end:]

while r:
	r = sock.recv(50)
	bucket += r
	analyse_data()

sock.close()
