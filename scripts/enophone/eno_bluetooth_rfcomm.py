#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""PyBluez simple example rfcomm-client.py
Simple demonstration of a client application that uses RFCOMM sockets intended
for use with rfcomm-server.
Author: Albert Huang <albert@csail.mit.edu>
$Id: rfcomm-client.py 424 2006-08-24 03:35:54Z albert $

Adjusted by Mindset Innovation, Inc. for connecting to enophones
- Changed to report streaming information in console
- Adjust host Mac Address to the address of your enophones
- Pipe the result into a file or through the conversion from binary to volts

"""

import sys, time

import bluetooth

# Python 2 compatibility
try:
    input = raw_input
except NameError:
    pass  # Python 3

port = 1
#host = "f4:0e:11:75:6d:53" #THIS SHOULD BE YOUR ENOPHONE MAC ADDRESS //David D.
host = "F4:0E:11:75:6F:E6"


# Create the client socket
sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((host, port))

print("Connected. Receiving...")

t0 = time.time()
prev_d = int(time.time() - t0)
r = sock.recv(50)
nsofar = len(r)
nnewlines = r.count(b'\n')
should_reset = False

while r:
    if should_reset and  time.time() - t0 > 10:
        sys.stderr.write(">> reset counters\n")
        sys.stderr.flush()
        t0 = time.time()
        nsofar = len(r)
        nnewlines = r.count(b'\n')
        should_reset = False
    sys.stdout.buffer.write(r)
    sys.stdout.flush()
    r = sock.recv(50)
    nsofar += len(r)
    nnewlines += r.count(b'\n')
    d = int(time.time() - t0)
    if d != prev_d:
        prev_d = d
        msg = "after {:.2f}s: {} bytes ({:.2f}Bps) {} lines ({:.5f}l/s)\n"
        t = time.time() - t0
        msg = msg.format(t, nsofar, nsofar/t, nnewlines, nnewlines/t)
        sys.stderr.write(msg)
        sys.stderr.flush()

sock.close()
