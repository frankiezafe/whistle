# enophone
This is the most basic toolkit to interface with the enophones.

- eno_bluetooth_rfcomm.py: Creates a bluetooth socket connection with your device using MAC Address
- binStream2csv.py: Converts the binary data stream to volts
- data_processing.py: Basic signal processing toolkit
- EEG_Blinks_4sec.csv: Sample data of EEG with blinks (muscle activity from the eyes) every 4 seconds
- HR_CH3-4.csv: Sample Heart Rate data recorded from the top two sensors of the enophone (other channels are floating -- do not contain data)

## CREATING A CONNECTION

1) In eno_bluetooth_rfcomm.py, adjust the MAC Address to match the enophone you want to use
2) The script will simply start streaming and printing data
3) To save the data you can simply send it into a file


Example command 1: This command will continuously print raw data from the headphones
```python
python eno_bluetooth_rfcomm.py
```


Example command 2: This command will save raw data into "testfile" and print streaming information
```python
python eno_bluetooth_rfcomm.py > testfile
```
Note: The bluetooth librairie used is pybluez in the sample code provided


## CONVERTING THE DATA FROM BINARY TO VOLTS

The raw data coming from the enophone is in binary format and needs to be converted using binStream2csv.

Example command 3: This command will use the data from testfile, convert it to volts and store it into a CSV file for later use.
```python
type testfile | python binStream2csv.py > testfile.csv
```
Note: Change type for cat on Linux/MacOS. 

Example command 4: Combine commands to create a connection, convert and save it into a file all at once.
```python
python eno_bluetooth_rfcomm.py | python binStream2csv.py > testfile.csv
```


## Processing and graphing the data

Using data_processing.py you can perform basic signal processing on the data to filter and reference the data.

Example command 5 (EEG + BLINKS): Sample EEG Data Processing
```python
python data_processing.py EEG_Blinks_4sec.csv --bandpass_range 1 40 --bandstop_range 59 61 --reference 0 --filter_cut 250
```
Note: This command will take the sample file "EEG_Blinks_4sec.csv", apply a bandpass filter between 1-40Hz, apply a bandstop filter for 60Hz mains, reference the channels with respect to channel 0 (right cushion channel) and removing 250 samples before and after the data to remove filter effecsts.

Example command 6 (HEART RATE): Sample Heart Rate Data Processing
```python
python data_processing.py HR_CH3-4.csv --bandpass_range 1 40 --band_stop_range 59 61 --reference 0 --filter_cut 250 --channels 0 3 4
```
Note: Similar to command 5, except we only use the channels 3 and 4 where the heart rate was collected. In this case a reference 0 means the first of the two channels selected. In the command --channels 0 3 4, 0 is the index and should always be there, 3 and 4 are the channels under consideration.
