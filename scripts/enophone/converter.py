#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
binStream2csv.py

Convert a binary stream of data from the enophone into volts

:copyright: © 2019-2020 Mindset Innovation, inc.  All rights reserved
:license: Proprietary
"""

import sys

import numpy as np

def align(instream, outstream, errstream, acc=b'', c=None):
    c = c or instream.buffer.read(1)
    while c != b'b':
        acc += c
        c = instream.buffer.read(1)
    c = instream.buffer.read(1)
    if c != b'S':
        acc += c
        return align(instream, outstream, errstream, acc, c)
    if acc:
        errstream.write("Skipped:[{}]\n".format(acc))
        errstream.flush()
    return

def doit(instream, outstream, errstream):
    chunksz = 25
    align(instream, outstream, errstream)
    buf = b'bS' + instream.buffer.read(20*chunksz - 2)
    while True:
        vals = np.frombuffer(buf, dtype='>i4').reshape(chunksz,5)
        v2 = np.zeros((chunksz,5))
        if any(vals[:,0] // 2**16 != 25171):
            errstream.write("Not 'bS', that's BS!\n")
            errstream.flush()
        v2[:,0] = vals[:,0] % 2**16
        for i in range(4):
            if any(vals[:,i+1] % 256 != i+16):
                errstream.write("Not '1{}', that's BS!\n".format(i))
                errstream.flush()
            v2[:,i+1] = (vals[:,i+1] // 256) * 5. / 2**23
        for v in v2:
            outstream.write(','.join(map(str, v)) + '\n')
        align(instream, outstream, errstream)
        buf = b'bS' + instream.buffer.read(20*chunksz - 2)

f_input = open('enotest','rb')
f_output = open('enotest.csv','wb')
f_err = open('enotest_err','wb')

doit( f_input, f_output, f_err )
