#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
electronics_noise_test.py

Copyright (c) 2019-2020 Mindset Innovation, inc.  All rights reserved
:license: Proprietary
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.signal

def filter(signal, lowerbound, upperbound, nyquist_f, filtertype, order):
    bandpass_filter_window = np.array([lowerbound, upperbound]) / nyquist_f

    a, b = scipy.signal.butter(N=order, Wn=bandpass_filter_window, btype=filtertype)
    filtered = scipy.signal.filtfilt(a, b, signal)
    return filtered

def doit(eegfile, samplerate, channels, bandpass_range, bandstop_range,
         sample_range, fft_win, reference, signal_frequency, filter_cut):
    data = np.loadtxt(eegfile, comments='%', delimiter=',', ndmin=2,
                      usecols=channels)


    ### CONVERT TO uV ###
    data[:,1:] = data[:,1:] / 100 * 10**6 #conversion to uV

    ### ARRAY SIZE ###
    if sample_range:
        data = data[sample_range[0]:sample_range[1]]
    nsamples = len(data)

    ### CHECK FOR SKIPPED ROWS IN DATA ###
    for i in range (0,len(data)-1):
        if (int(data[i+1,0]) - int(data[i,0]) != 1):
            print("There's a skipped row in this range at line", i, "data index ", int(data[i,0]/1E6))
    data = data[:,1:]
    nchannels = len(channels)

    ### REREFERENCING ###
    if reference is not None:
        data -= data[:, reference]
        data = np.delete(data, reference, axis=1)
        nchannels -= 1

    DC_LEVEL = np.zeros(4)

    #Compute DC level
    for i in range(0, nchannels-1):
        DC_LEVEL[i] = np.mean(data[:,i])
        #data[:,i] = data[:,i] - DC_LEVEL[i]

    ### FILTERING ###
    if bandpass_range:
        data = data.T
        for i in range(0, nchannels-1):
            #you can adjust filters according to your requirement
            data[i] = filter(data[i], bandpass_range[0], bandpass_range[1], samplerate/2, filtertype='bandpass', order=4)

        data = data.T
        if filter_cut:
            data = data[filter_cut:len(data)-filter_cut]
            nsamples = len(data)

    if bandstop_range:
        data = data.T
        for i in range(0, nchannels-1):
            data[i] = filter(data[i], bandstop_range[0], bandstop_range[1], samplerate/2, filtertype='bandstop', order=4)
        data = data.T
        if filter_cut:
            data = data[filter_cut:len(data)-filter_cut]
            nsamples = len(data)

    ### CALCULATE MAGNITUDE OF SIGNAL & FILTER SIGNAL FOR NOISE CALCULATIONS ###
    if signal_frequency:
        signal_width = 2.0
        print("whole signal RMS:", np.mean(data.std(axis=0)))
        data = data.T
        for i in range(0, nchannels-1):
            data[i] = filter(data[i], signal_frequency - signal_width/2, signal_frequency + signal_width/2, samplerate/2, filtertype='bandstop', order=4)
        data = data.T
        if filter_cut:
            data = data[filter_cut:len(data)-filter_cut]
            nsamples = len(data)

    ### FFT ###
    win = {'none' : 1,
           'hanning' : np.hanning(nsamples),
           'hamming' : np.hamming(nsamples)}[fft_win]
    win = np.array([win]).T
    fft = np.abs(np.fft.rfft(data*win, axis=0, norm=None)) * 2 / nsamples
    freqs = np.fft.rfftfreq(nsamples, 1./samplerate)
    ix = np.ones((len(freqs),), dtype=bool)

    ### CALCULATIONS ###
    start = 0
    stop = len(data)/250
    interval = 1/250
    #interval = 1
    t = np.arange(start,stop,interval)

    #Printing channel values (remove commenting on values of interest)
    #print('RMS A1/LEFT CUSHION:', np.mean(data[:,1].std(axis=0)))
    #print('RMS C3/LEFT TOP:', np.mean(data[:,3].std(axis=0)))
    #print('RMS C4/RIGHT TOP:', np.mean(data[:,2].std(axis=0)))
    #print('RMS A2/RIGHT CUSHION:', np.mean(data[:,0].std(axis=0)))
    #print('uVpp A1/LEFT CUSHION:', np.mean(data[:,1].max(axis=0)-data[:,1].min(axis=0)))
    #print('uVpp C3/LEFT TOP:', np.mean(data[:,3].max(axis=0)-data[:,3].min(axis=0)))
    #print('uVpp C4/RIGHT TOP:', np.mean(data[:,2].max(axis=0)-data[:,2].min(axis=0)))
    #print('uVpp A2/RIGHT CUSHION:', np.mean(data[:,0].std(axis=0))*2*np.sqrt(2))
    #print('DC Level A1/LEFT CUSHION:', DC_LEVEL[1])
    #print('DC Level C3/LEFT TOP:', DC_LEVEL[3])
    #print('DC Level C4/RIGHT TOP:', DC_LEVEL[2])
    #print('DC Level A2/RIGHT CUSHION:', DC_LEVEL[0])

    #plot signals
    #The channel order is A2 (Right Cushion), A1 (Left Cushion), C4 (Right Top), C3 (Left Top)
    #If you reference to any specific channel, you will have to adjust the legend
    plt.plot(t,data,alpha=1,markevery=5)
    plt.legend(('A2 (Right)', 'A1 (Left)','C4 (Top Right)','C3 (Top Left)'))
    plt.xlabel('Time (sec)')
    #plt.ylabel('Impedance (kOhms)')
    plt.ylabel(' Voltage (uV)')
    plt.show()

    plt.plot(freqs[ix], fft[ix])
    plt.legend(('A2 (Right)', 'A1 (Left)','C4 (Top Right)','C3 (Top Left)'))
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    #plt.legend(('Alpha','LEFT','TOP RIGHT','TOP LEFT'))
    plt.show()

    exit(0)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='calc powers')
    parser.add_argument('eegfile', metavar='eegfile', type=str,
                        help='filename for the EEG CSV.')
    parser.add_argument('--samplerate', type=float, default=250,
                        help='sample rate (Hz.)')
    parser.add_argument('-c', '--channels', type=int, nargs='+',
                        default=[0,1,2,3,4],
                        help='channels (column numbers) containing an actual signal')
    parser.add_argument('--bandstop_range', type=float, default=None, nargs=2,
                        help='bandstop range (Hz.)')
    parser.add_argument('--sample_range', type=int, default=None, nargs=2,
                        help='use only samples between these two')
    parser.add_argument('--bandpass_range', type=float, default=None, nargs=2,
                        help='freq range [a,b] (Hz.)')
    parser.add_argument('--fft_win', type=str, default='hanning',
                        choices=['none', 'hanning', 'hamming'],
                        help='window to use prior to FFT.')
    parser.add_argument('--reference', type=int, default=None, nargs=1,
                        help='channel you want to reference to: [0,1,2,3] or None')
    parser.add_argument('--signal_frequency', type=float, default=None,
                        help='frequency of the signal injected, in Hz')
    parser.add_argument('--filter_cut', type=int, default=None,
                        help='number of data points to cut from each end of the signal after filtering')

    args = parser.parse_args()

    doit(args.eegfile, args.samplerate, args.channels, args.bandpass_range,
         args.bandstop_range, args.sample_range, args.fft_win, args.reference, args.signal_frequency, args.filter_cut)
