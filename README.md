raspberry notes

godot: https://github.com/hiulit/Unofficial-Godot-Engine-Raspberry-Pi

`scons platform=x11 target=release_debug tools=yes use_llvm=yes CCFLAGS="-mtune=cortex-a72 -mcpu=cortex-a72 -mfloat-abi=hard -mlittle-endian -munaligned-access -mfpu=neon-fp-armv8" -j4`

overclocking: https://rpihacks.wordpress.com/2014/11/27/raspberry-pi-optimization-guide/

vulkan:

ref: https://www.youtube.com/watch?v=TLzFPIoHhS8

`$ sudo nano /etc/apt/sources.list`

uncomment last line (non free repo)

```
$ sudo apt update
$ sudo apt build-dep mesa
$ sudo apt install vulkan-tools
$ sudo apt install libxcb-shm0-dev
```

```
$ git clone https://gitlab.freedesktop.org/mesa/mesa.git
$ cd mesa
$ mkdir build
$ cd build
$ meson --libdir lib -Dplatforms=x11 -Dvulkan-drivers=broadcom -Ddri-drivers= -Dgallium-drivers=v3d,kmsro,vc4 -Dbuildtype=debug ..
$ sudo ninja install
$ vkcube
```

vulkan demo: https://blogs.igalia.com/apinheiro/2020/06/v3dv-quick-guide-to-build-and-run-some-demos/

